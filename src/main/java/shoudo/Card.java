package shoudo;

import java.util.Objects;


/**
 * Information expert for card objects in the Shoudo game. Contains gameplay-
 * relevant information and image paths.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class Card {

    /**
     * The terrain modifier that this card provides to an army standing on it
     * in combat.
     */
    private int terrainModifier;
    /**
     * The clan this card is associated with; used when burned in conjunction
     * with {@link Card#pieceSupported} to determine what army is wounded.
     */
    private String clan;
    /**
     * The army type this card supports; used when burned in conjunction with
     * {@link Card#clan} to determine what army is wounded.
     */
    private String pieceSupported;
    /**
     * Whether this card is a castle (a type of card which acts as the start
     * location for the army it supports and as the supply center for that
     * army).
     */
    private Boolean isCastle;
    /**
     * The path to the unburned image for this card, which will be used by the
     * ui package to load graphics.
     */
    private String image;
    /**
     * The path to the burned image for this card, which will be used by the ui
     * package to load graphics.
     */
    private String burnedImage;

    /**
     * Package-private default constructor. Prevents instantiation outside of
     * package scope while allowing generic instances within package.
     */
    Card() {

    }

    /**
     * Package-private custom constructor. Prevents instantiation outside of
     * package scope and allows creation of objects with defined values within
     * package (used in {@link GameRunner}.
     *
     * @param tm initial terrain modifier
     * @param c initial clan
     * @param ps initial piece supported
     * @param ic initial isCastle value
     * @param i initial unburned image path
     * @param bi initial burned image path
     */
    Card(final int tm,
         final String c,
         final String ps,
         final Boolean ic,
         final String i,
         final String bi) {
        this.terrainModifier = tm;
        this.clan = c;
        this.pieceSupported = ps;
        this.isCastle = ic;
        this.image = i;
        this.burnedImage = bi;
    }

    /**
     * @return {@link Card#terrainModifier}
     */
    public final int getTerrainModifier() {
        return terrainModifier;
    }

    /**
     * Sets the new terrain modifier.
     *
     * @param tm new terrain modifier value
     */
    public final void setTerrainModifier(final int tm) {
        this.terrainModifier = tm;
    }

    /**
     * @return {@link Card#clan}
     */
    public final String getClan() {
        return clan;
    }

    /**
     * Sets new clan value.
     *
     * @param c new clan value
     */
    public final void setClan(final String c) {
        this.clan = c;
    }

    /**
     * @return {@link Card#pieceSupported}
     */
    public final String getPieceSupported() {
        return pieceSupported;
    }

    /**
     * Sets new piece supported.
     *
     * @param ps new value for piece supported
     */
    public final void setPieceSupported(final String ps) {
        this.pieceSupported = ps;
    }

    /**
     * @return {@link Card#isCastle}
     */
    public final Boolean getIsCastle() {
        return this.isCastle;
    }

    /**
     * Sets new isCastle value.
     *
     * @param ic new value for isCastle
     */
    public final void setIsCastle(final Boolean ic) {
        this.isCastle = ic;
    }

    /**
     * @return {@link Card#image}
     */
    public final String getImage() {
        return image;
    }

    /**
     * Sets new unburned image for this card.
     *
     * @param i new value for image
     */
    public final void setImage(final String i) {
        this.image = i;
    }

    /**
     * @return {@link Card#burnedImage}
     */
    public final String getBurnedImage() {
        return this.burnedImage;
    }

    /**
     * Sets new burned image for this card.
     *
     * @param bi new value for burned image
     */
    public final void setBurnedImage(final String bi) {
        this.burnedImage = bi;
    }

    /**
     * equals method for Card; uses standard equals implementation for all
     * fields.
     * @param o object to compare against
     * @return whether the objects are equal
     */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Card card = (Card) o;
        return terrainModifier == card.terrainModifier
                && Objects.equals(clan, card.clan)
                && Objects.equals(pieceSupported, card.pieceSupported)
                && Objects.equals(isCastle, card.isCastle)
                && Objects.equals(image, card.image)
                && Objects.equals(burnedImage, card.burnedImage);
    }

    /**
     * hashCode method for Card; uses standard Objects.hash() functionality
     * for all fields.
     *
     * @return hash code for this Card
     */
    @Override
    public final int hashCode() {

        return Objects.hash(terrainModifier,
                clan,
                pieceSupported,
                isCastle,
                image,
                burnedImage);
    }
}

package shoudo;

import awsutil.User;


/**
 * Information expert for a Shoudo game player. Stores relevant game
 * information and can derive additional properties.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class Player {

    /**
     * User to record stats for (can be null -- then no records).
     */
    private User user;
    /**
     * Player's initiative; used to determine turn order and clan played.
     */
    private Integer initiative;

    /**
     * Static final value to associate initiative 1 with Uesugi clan.
     */
    private static final int UESUGI = 1;
    /**
     * Static final value to associate initiative 2 with Mori clan.
     */
    private static final int MORI = 2;
    /**
     * Static final value to associate initiative 3 with Tokugawa clan.
     * Shimazu clan is assigned if none of these values is present (which
     * can only occur if Player has initiative 4)
     */
    private static final int TOKUGAWA = 3;

    /**
     * Default constructor. Initializes initiative to 0 (an invalid number)
     * and user to null;
     */
    public Player() {
        this.initiative = 0;
        this.user = null;
    }

    /**
     * @return {@link Player#user}
     */
    public final User getUser() {
        return user;
    }

    /**
     * Sets new user for stats tracking.
     *
     * @param u user to save stats for
     */
    public final void setUser(final User u) {
        this.user = u;
    }

    /**
     * Derives user's clan based on initiative value:
     *
     * 1 - Uesugi
     * 2 - Mori
     * 3 - Tokugawa
     * 4 - Shimazu
     *
     * This is used in other code to verify actions such as burn and attack.
     *
     * @return String containing clan name
     */
    public final String getClan() {
        switch (this.initiative) {
            case UESUGI:
                return "Uesugi";
            case MORI:
                return "Mori";
            case TOKUGAWA:
                return "Tokugawa";
            default:
                return "Shimazu";
        }
    }

    /**
     * @return {@link Player#initiative}
     */
    public final Integer getInitiative() {
        return initiative;
    }

    /**
     * Sets new initiative value.
     *
     * @param i new value for initiative
     */
    public final void setInitiative(final Integer i) {
        this.initiative = i;
    }
}

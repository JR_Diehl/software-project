package shoudo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import static shoudo.GameRunner.State.LOAD;


/**
 * Manager for game state and controller of important game information such as
 * list of armies, game board, and both players. Implemented as a singleton to
 * prevent multiple games from attempting to run simultaneously; this also
 * enables easy killing of a game via the {@link GameRunner#killInstance()}
 * method.
 *
 * @author JR Diehl
 * @version 1.0
 */
public final class GameRunner {

    /**
     * State enum to allow for easier tracking of current game state.
     *
     * @author JR Diehl
     * @version 1.0
     */
    public enum State {
        /**
         * State for when game is loading information and UI.
         */
        LOAD,
        /**
         * State for player 1's turn. Only player 1 can interact with board in
         * this state.
         */
        PLAYER1_TURN,
        /**
         * State for player 2's turn. Only player 2 can interact with board in
         * this state.
         */
        PLAYER2_TURN,
        /**
         * State for when a player is selecting. Only the active player is able
         * to provide input in this state.
         */
        SELECT
    }

    // singleton fields/methods
    /**
     * Singleton instance.
     */
    private static GameRunner instance = null;

    /**
     * Private constructor to prevent external instantiation except via
     * {@link GameRunner#getInstance()}, initializes gameState to LOAD,
     * lastState to null, and all other fields to generic instances of
     * their respective classes.
     */
    private GameRunner() {
        this.gameState = LOAD;
        this.lastState = null;
        this.armies = new ArrayList<>();
        this.gameBoard = new Board();
        this.player1 = new Player();
        this.player2 = new Player();
    }

    /**
     * Gets the singleton instance, creating it if it is null. Does not
     * use synchronized double-locking since this application is not
     * multithreaded.
     *
     * @return singleton GameRunner instance (current or new)
     */
    public static GameRunner getInstance() {
        if (instance == null) {
            instance = new GameRunner();
        }
        return instance;
    }

    /**
     * Kills the current singleton instance, effectively quitting a game by
     * deleting its data.
     */
    public static void killInstance() {
        instance = null;
    }

    // constants
    /**
     * Static final value to store number of columns.
     */
    private static final Integer COLS = 8;
    /**
     * Static final value to store number of rows.
     */
    private static final Integer ROWS = 5;

    /**
     * Game board for current game being played. Is a 2D list of Card objects
     * effectively.
     */
    private Board gameBoard;
    /**
     * List of living armies on the game board.
     */
    private List<Army> armies;
    /**
     * Player who assumes the role of player 1 (this does not mean that they
     * will take the first turn). This player is always the logged in user.
     */
    private Player player1;
    /**
     * Player who assumes the role of player 2 (this does not mean that they
     * will take the second turn). This player is always the unregistered
     * opponent.
     */
    private Player player2;

    /**
     * Stores the current state of the game for ease of practice in gameplay
     * loop.
     */
    private State gameState;
    /**
     * Stores the previous game state so that it can be reverted.
     */
    private State lastState;

    /**
     * Generates a new game board by first creating a cardsList xml, then
     * loading that xml into a list of Cards. This list is then shuffled
     * and placed into all of the (row,col) positions.
     *
     * @return the randomly generated game board
     */
    public Board generateNewBoard() {
        List<Card> cards;
        try {
            createCardsXml();
            cards = CardsList.loadFromXml();
        } catch (JAXBException | IOException e) {
            cards = new ArrayList<>();
        }

        Collections.shuffle(cards);
        int i = 0;
        List<List<Card>> board = new ArrayList<>();
        for (int j = 0; j < ROWS; j++) {
            board.add(new ArrayList<>());
            for (int k = 0; k < COLS; k++) {
                board.get(j).add(new Card());
            }
        }

        Card card;
        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                card = cards.get(i);
                i++;
                board.get(r).get(c).setBurnedImage(
                                card.getBurnedImage());
                board.get(r).get(c).setClan(card.getClan());
                board.get(r).get(c).setImage(
                                card.getImage());
                board.get(r).get(c).setIsCastle(card.getIsCastle());
                board.get(r).get(c).setPieceSupported(
                        card.getPieceSupported());
                board.get(r).get(c).setTerrainModifier(
                        card.getTerrainModifier());
            }
        }

        this.gameBoard = new Board(board);

        return this.gameBoard;
    }

    /**
     * Creates a new cards xml locally, using relative file paths for images
     * that will be converted to absolute on image load (to avoid stale paths).
     *
     * @throws JAXBException if JAXB encounters an error
     */
    private void createCardsXml() throws JAXBException {
        CardsList cards = new CardsList();

        cards.add(new Card(0, "Uesugi", "Samurai", false,
                "/images/cards/card_Page_17.jpg",
                "/images/cards/card_Page_18.jpg"));
        cards.add(new Card(0, "Uesugi", "Heir", false,
                "/images/cards/card_Page_15.jpg",
                "/images/cards/card_Page_16.jpg"));
        cards.add(new Card(1, "Uesugi", "Heir", false,
                "/images/cards/card_Page_13.jpg",
                "/images/cards/card_Page_14.jpg"));
        cards.add(new Card(0, "Uesugi", "Daimyo", false,
                "/images/cards/card_Page_11.jpg",
                "/images/cards/card_Page_12.jpg"));
        cards.add(new Card(1, "Uesugi", "Daimyo", false,
                "/images/cards/card_Page_09.jpg",
                "/images/cards/card_Page_10.jpg"));
        cards.add(new Card(2, "Uesugi", "Daimyo", false,
                "/images/cards/card_Page_07.jpg",
                "/images/cards/card_Page_08.jpg"));
        cards.add(new Card(2, "Uesugi", "Samurai", true,
                "/images/cards/card_Page_05.jpg",
                "/images/cards/card_Page_06.jpg"));
        cards.add(new Card(2, "Uesugi", "Heir", true,
                "/images/cards/card_Page_03.jpg",
                "/images/cards/card_Page_04.jpg"));
        cards.add(new Card(2, "Uesugi", "Daimyo", true,
                "/images/cards/card_Page_01.jpg",
                "/images/cards/card_Page_02.jpg"));
        cards.add(new Card(0, "Uesugi", "Shrine", false,
                "/images/cards/card_Page_19.jpg",
                "/images/cards/card_Page_20.jpg"));
        cards.add(new Card(0, "Mori", "Samurai", false,
                "/images/cards/card_Page_77.jpg",
                "/images/cards/card_Page_78.jpg"));
        cards.add(new Card(0, "Mori", "Heir", false,
                "/images/cards/card_Page_75.jpg",
                "/images/cards/card_Page_76.jpg"));
        cards.add(new Card(1, "Mori", "Heir", false,
                "/images/cards/card_Page_73.jpg",
                "/images/cards/card_Page_74.jpg"));
        cards.add(new Card(0, "Mori", "Daimyo", false,
                "/images/cards/card_Page_71.jpg",
                "/images/cards/card_Page_72.jpg"));
        cards.add(new Card(1, "Mori", "Daimyo", false,
                "/images/cards/card_Page_69.jpg",
                "/images/cards/card_Page_70.jpg"));
        cards.add(new Card(2, "Mori", "Daimyo", false,
                "/images/cards/card_Page_67.jpg",
                "/images/cards/card_Page_68.jpg"));
        cards.add(new Card(2, "Mori", "Samurai", true,
                "/images/cards/card_Page_65.jpg",
                "/images/cards/card_Page_66.jpg"));
        cards.add(new Card(2, "Mori", "Heir", true,
                "/images/cards/card_Page_63.jpg",
                "/images/cards/card_Page_64.jpg"));
        cards.add(new Card(2, "Mori", "Daimyo", true,
                "/images/cards/card_Page_61.jpg",
                "/images/cards/card_Page_62.jpg"));
        cards.add(new Card(0, "Mori", "Shrine", false,
                "/images/cards/card_Page_79.jpg",
                "/images/cards/card_Page_80.jpg"));
        cards.add(new Card(0, "Tokugawa", "Samurai", false,
                "/images/cards/card_Page_57.jpg",
                "/images/cards/card_Page_58.jpg"));
        cards.add(new Card(0, "Tokugawa", "Heir", false,
                "/images/cards/card_Page_55.jpg",
                "/images/cards/card_Page_56.jpg"));
        cards.add(new Card(1, "Tokugawa", "Heir", false,
                "/images/cards/card_Page_53.jpg",
                "/images/cards/card_Page_54.jpg"));
        cards.add(new Card(0, "Tokugawa", "Daimyo", false,
                "/images/cards/card_Page_51.jpg",
                "/images/cards/card_Page_52.jpg"));
        cards.add(new Card(1, "Tokugawa", "Daimyo", false,
                "/images/cards/card_Page_49.jpg",
                "/images/cards/card_Page_50.jpg"));
        cards.add(new Card(2, "Tokugawa", "Daimyo", false,
                "/images/cards/card_Page_47.jpg",
                "/images/cards/card_Page_48.jpg"));
        cards.add(new Card(2, "Tokugawa", "Samurai", true,
                "/images/cards/card_Page_45.jpg",
                "/images/cards/card_Page_46.jpg"));
        cards.add(new Card(2, "Tokugawa", "Heir", true,
                "/images/cards/card_Page_43.jpg",
                "/images/cards/card_Page_44.jpg"));
        cards.add(new Card(2, "Tokugawa", "Daimyo", true,
                "/images/cards/card_Page_41.jpg",
                "/images/cards/card_Page_42.jpg"));
        cards.add(new Card(0, "Tokugawa", "Shrine", false,
                "/images/cards/card_Page_59.jpg",
                "/images/cards/card_Page_60.jpg"));
        cards.add(new Card(0, "Shimazu", "Samurai", false,
                "/images/cards/card_Page_37.jpg",
                "/images/cards/card_Page_38.jpg"));
        cards.add(new Card(0, "Shimazu", "Heir", false,
                "/images/cards/card_Page_35.jpg",
                "/images/cards/card_Page_36.jpg"));
        cards.add(new Card(1, "Shimazu", "Heir", false,
                "/images/cards/card_Page_33.jpg",
                "/images/cards/card_Page_34.jpg"));
        cards.add(new Card(0, "Shimazu", "Daimyo", false,
                "/images/cards/card_Page_31.jpg",
                "/images/cards/card_Page_32.jpg"));
        cards.add(new Card(1, "Shimazu", "Daimyo", false,
                "/images/cards/card_Page_29.jpg",
                "/images/cards/card_Page_30.jpg"));
        cards.add(new Card(2, "Shimazu", "Daimyo", false,
                "/images/cards/card_Page_27.jpg",
                "/images/cards/card_Page_28.jpg"));
        cards.add(new Card(2, "Shimazu", "Samurai", true,
                "/images/cards/card_Page_25.jpg",
                "/images/cards/card_Page_26.jpg"));
        cards.add(new Card(2, "Shimazu", "Heir", true,
                "/images/cards/card_Page_23.jpg",
                "/images/cards/card_Page_24.jpg"));
        cards.add(new Card(2, "Shimazu", "Daimyo", true,
                "/images/cards/card_Page_21.jpg",
                "/images/cards/card_Page_22.jpg"));
        cards.add(new Card(0, "Shimazu", "Shrine", false,
                "/images/cards/card_Page_39.jpg",
                "/images/cards/card_Page_40.jpg"));

        JAXBContext context = JAXBContext.newInstance(CardsList.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        File file = new File("./cardsList.xml");
        marshaller.marshal(cards, file);
    }

    /**
     * Controls changing of state, ensuring that lastState always contains
     * the previous gameState.
     *
     * @param newState new value for gameState
     */
    public void changeState(final State newState) {
        this.lastState = this.gameState;
        this.gameState = newState;
    }

    /**
     * @return {@link GameRunner#gameState}
     */
    public State getGameState() {
        return gameState;
    }

    /**
     * @return {@link GameRunner#lastState}
     */
    public State getLastState() {
        return lastState;
    }

    /**
     * @return {@link GameRunner#armies}
     */
    public List<Army> getArmies() {
        return this.armies;
    }

    /**
     * @return {@link GameRunner#player1}
     */
    public Player getPlayer1() {
        return player1;
    }

    /**
     * Sets a new Player as player 1.
     *
     * @param p1 new player 1
     */
    public void setPlayer1(final Player p1) {
        this.player1 = p1;
    }

    /**
     * @return {@link GameRunner#player2}
     */
    public Player getPlayer2() {
        return player2;
    }

    /**
     * Sets a new Player as player 2.
     *
     * @param p2 new player 2
     */
    public void setPlayer2(final Player p2) {
        this.player2 = p2;
    }
}

package shoudo;

import awsutil.User;

import java.util.Random;


/**
 * Builder class for player objects. Used to expedite player creation during
 * game initialization.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class PlayerBuilder {

    /**
     * Player object which is built onto.
     */
    private Player player;

    /**
     * Static final value containing number of clans available.
     */
    private static final int NUMCLANS = 4;

    /**
     * Default constructor. Instantiates {@link PlayerBuilder#player} as a new
     * default Player.
     */
    public PlayerBuilder() {
        this.player = new Player();
    }

    /**
     * @return {@link PlayerBuilder#player}
     */
    public final Player getPlayer() {
        return this.player;
    }

    // builder methods

    /**
     * Builds a default player and returns this object so more elements can be
     * added. Useful to begin building, though not very interesting.
     *
     * @return this object
     */
    public final PlayerBuilder buildDefaultPlayer() {
        this.player = new Player();
        return this;
    }

    /**
     * Builds a user into the player object and returns this object for more
     * chaining.
     *
     * @param user user to be attached to the internal player
     * @return this object (player has had a user added to it)
     */
    public final PlayerBuilder withUser(final User user) {
        this.player.setUser(user);
        return this;
    }

    /**
     * Builds a specific initiative into the player and returns this object
     * for more chaining.
     *
     * @param init initiative to set in player
     * @return this object (player has had initiative assigned)
     */
    public final PlayerBuilder withInitiative(final Integer init) {
        this.player.setInitiative(init);
        return this;
    }

    /**
     * Builds a random initiative ranging from 1 to
     * {@link PlayerBuilder#NUMCLANS} into the player and returns this object
     * for more chaining.
     *
     * @return this object (player has a random valid initiative)
     */
    public final PlayerBuilder withRandomInitiative() {
        Random rand = new Random();
        int init = rand.nextInt() % NUMCLANS;
        if (init < 0) {
            init = init * -1;
        }
        init++;
        this.player.setInitiative(init);
        return this;
    }

}

package shoudo;

import java.util.Objects;


/**
 * Stores relevant game information about an Army piece for the Shoudo game
 * as an information expert.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class Army {

    /**
     * The Army's unit type; either Samurai, Heir, or Daimyo.
     */
    private String type;
    /**
     * The Army's strength, which is used as a bonus during combat.
     */
    private Integer strength;
    /**
     * The Army's row coordinate on the game board. Ranges from 0 to 4.
     */
    private Integer row;
    /**
     * The Army's column coordinate on the game board. Ranges from 0 to 7.
     */
    private Integer col;
    /**
     * The Army's owner. This is used throughout the application as a way to
     * determine the clan of the Army indirectly.
     */
    private Player owner;

    /**
     * Default constructor; initializes owner to null, type to empty, and all
     * other values to 0.
     */
    public Army() {
        this.type = "";
        this.strength = 0;
        this.col = 0;
        this.row = 0;
        this.owner = null;
    }

    /**
     * @return {@link Army#type}
     */
    public final String getType() {
        return type;
    }

    /**
     * Sets the Army's unit type to a new unit type.
     *
     * @param t new value for type
     */
    public final void setType(final String t) {
        this.type = t;
    }

    /**
     * @return {@link Army#strength}
     */
    public final Integer getStrength() {
        return strength;
    }

    /**
     * Adjusts the Army's strength; used in the Burn and Restore actions
     * in the {@link Actions} class to increase or decrease Army strengths.
     *
     * @param s new value for strength
     */
    public final void setStrength(final Integer s) {
        this.strength = s;
    }

    /**
     * @return {@link Army#row}
     */
    public final Integer getRow() {
        return row;
    }

    /**
     * Sets the row coordinate of the Army. Used to track position, but not
     * for explicit movement.
     *
     * @param r new value for row
     */
    public final void setRow(final Integer r) {
        this.row = r;
    }

    /**
     * @return {@link Army#col}
     */
    public final Integer getCol() {
        return col;
    }

    /**
     * Sets the column coordinate of the Army. Used to track position, but not
     * for explicit movement.
     *
     * @param c new value for col
     */
    public final void setCol(final Integer c) {
        this.col = c;
    }

    /**
     * @return {@link Army#owner}
     */
    public final Player getOwner() {
        return this.owner;
    }

    /**
     * Sets the owner. Should not be used outside of initialization as it can
     * create unclear/non-intuitive situations.
     *
     * @param o new value for owner
     */
    public final void setOwner(final Player o) {
        this.owner = o;
    }

    /**
     * equals method for Army; uses standard equals implementation for all
     * fields.
     * @param o object to compare against
     * @return whether the objects are equal
     */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Army army = (Army) o;
        return Objects.equals(type, army.type)
                && Objects.equals(strength, army.strength)
                && Objects.equals(row, army.row)
                && Objects.equals(col, army.col)
                && Objects.equals(owner, army.owner);
    }

    /**
     * hashCode method for Army; uses standard Objects.hash() functionality
     * for all fields.
     *
     * @return hash code for this Army
     */
    @Override
    public final int hashCode() {
        return Objects.hash(type, strength, row, col, owner);
    }
}

package shoudo;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Handles loading of a list of cards from an xml file and acts as a wrapper
 * around a list of cards so that it can be written/read to and from xml.
 *
 * @author JR Diehl
 * @version 1.0
 */
@XmlRootElement(name = "cardList")
class CardsList {

    /**
     * The list of cards being wrapped by this class.
     */
    @XmlElementWrapper(name = "cards")
    @XmlElement(name = "card")
    private final List<Card> cards;

    /**
     * Package-private default constructor. Restricts instantiation to this
     * package only, while allowing instances within shoudo package.
     */
    CardsList() {
        this.cards = new ArrayList<>();
    }

    /**
     * Exposes the list add function for the wrapped list.
     *
     * @param card card to add to the list
     */
    public void add(final Card card) {
        this.cards.add(card);

    }

    /**
     * Loads a list of cards from the project cardsList.xml path (directory
     * is assumed to be same as this class).
     *
     * @return a new list of cards loaded from the xml
     * @throws JAXBException if JAXB encounters an error
     * @throws IOException if there is a file I/O error
     */
    public static List<Card> loadFromXml() throws JAXBException, IOException {
        JAXBContext context = JAXBContext.newInstance(CardsList.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        FileReader reader = null;
        List<Card> xmlCards;
        try {
            String path = "./cardsList.xml";

            File file = new File(path);
            System.out.println(file.getAbsolutePath());

            reader = new FileReader(path);

            xmlCards = ((CardsList) unmarshaller.unmarshal(reader)).cards;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }


        return xmlCards;
    }

}

package shoudo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class provides utilities for managing a Shoudo game board.
 *
 * @author JR Diehl
 * @version 1.0
 */
@XmlRootElement(name = "board")
public class Board {

    /**
     * The 2D list that makes up the actual board. Indexing is (row)(col).
     */
    private final List<List<Card>> board;

    /**
     * Default constructor. Instantiates {@link Board#board} to a new ArrayList
     */
    Board() {
        this.board = new ArrayList<>();
    }

    /**
     * Overloaded constructor. Instantiates {@link Board#board} to b.
     *
     * @param b 2D list to use for instantiating board
     */
    Board(final List<List<Card>> b) {
        this.board = b;
    }

    /**
     * @return {@link Board#board}
     */
    public final List<List<Card>> getBoard() {
        return this.board;
    }

}

package shoudo;

import ui.AlertBox;
import ui.ArmySprite;
import ui.CardSprite;
import ui.ConfirmBox;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Utility class which provides functions that perform the three primary
 * actions that an army can perform in a Shoudo game. These are:
 *
 * - {@link Actions#attack(Army, Army, Card, Card, Boolean, Boolean)}
 * - {@link Actions#burn(CardSprite, List, Player)}
 * - {@link Actions#restore(CardSprite, List)}
 *
 * @author JR Diehl
 * @version 1.0
 */
public final class Actions {

    /**
     * Static final value to store maximum rounds in a combat initiated by
     * {@link Actions#attack(Army, Army, Card, Card, Boolean, Boolean)}.
     */
    private static final int MAX_RNDS = 3;
    /**
     * Static final value to store number of round victories necessary to
     * win a combat initiated by
     * {@link Actions#attack(Army, Army, Card, Card, Boolean, Boolean)}.
     */
    private static final int WIN_RNDS = 2;
    /**
     * Static final value to store the "die value" (the range of possible
     * random rolls from 1 to DIE_VAL inclusive) for combats.
     */
    private static final int DIE_VAL = 6;

    /**
     * Default constructor for this class. Hidden to prevent accidental or
     * malicious instantiation of this static utility class.
     */
    private Actions() {

    }

    /**
     * Initiates an attack from the attacker to the defender, assuming that the
     * attack has already been verified to be a valid attack by the calling
     * code. Executes up to three rounds, prompting the attacker with the
     * option to retreat after each round if canRetreat is true. Returns an
     * army representing the loser based on the result of combat --
     *
     * - attacker if attacker is defeated
     * - defender if defender is defeated
     * - new {@link Army} if draw
     * - null if attacker retreated
     *
     * This will be used by the calling code to possibly destroy an army
     * as a result of the combat.
     *
     * @param attacker the attacking army; can retreat if moves remaining
     * @param defender the defending army (or army acting as a proxy for a
     *                 castle)
     * @param attCard card on which attacker is resting
     * @param defCard card on which defender is resting (or which is defender
     *                if defender is a castle)
     * @param defenderIsNeutral whether defender is neutral or an opponent
     * @param canRetreat whether attacker can retreat
     * @return Army representing loser of combat
     */
    public static Army attack(final Army attacker, final Army defender,
                              final Card attCard,
                              final Card defCard,
                              final Boolean defenderIsNeutral,
                              final Boolean canRetreat) {
        int winsP1 = 0, winsP2 = 0, totalRnds = 0;
        Random rand = new Random();
        int roll1, roll2;
        String combatResults = "";
        Boolean stop = false;

        int attMod = attCard.getTerrainModifier() + attacker.getStrength();
        int defMod = defCard.getTerrainModifier();
        if (!defenderIsNeutral) {
            defMod += defender.getStrength();
        }

        StringBuilder buf = new StringBuilder();

        while (winsP1 < WIN_RNDS && winsP2 < WIN_RNDS
                && totalRnds < MAX_RNDS && !stop) {
            buf.append("Round ");
            buf.append(totalRnds + 1);
            buf.append(": ");
            roll1 = rand.nextInt() % DIE_VAL;
            roll2 = rand.nextInt() % DIE_VAL;
            if (roll1 < 0) {
                roll1 = roll1 * -1;
            }
            if (roll2 < 0) {
                roll2 = roll2 * -1;
            }
            roll1++;
            roll2++;
            roll1 += attMod;
            roll2 += defMod;
            buf.append(roll1);
            buf.append(" vs ");
            buf.append(roll2);
            buf.append("\n");
            if (roll1 > roll2) {
                winsP1++;
            } else if (roll1 < roll2) {
                winsP2++;
            }
            totalRnds++;

            if (totalRnds < MAX_RNDS && canRetreat) {
                if (ConfirmBox.display("Combat Results:",
                        buf.toString() + "Retreat?\n")) {
                    stop = true;
                }
            }
        }
        Army winner = null;
        if (stop) {
            buf.append("Attacker retreated!\n");
        } else if (winsP1 > winsP2) {
            buf.append("Attacker wins!\n");
            winner = attacker;
        } else if (winsP2 > winsP1) {
            buf.append("Defender wins!\n");
            winner = defender;
        } else {
            buf.append("Draw!\n");
            winner = new Army();
        }

        AlertBox.display("Combat Results:", buf.toString());

        return winner;
    }

    /**
     * Performs the Restore action for one clan's Shrine, which is assumed to
     * be the CardSprite passed by the calling code. This action allows each
     * of the Shrine's owner's armies to regain one strength. Actual flipping
     * of the CardSprite object is handled by the calling code.
     *
     * @param card the Shrine that was restored
     * @param armies a list of all living armies
     */
    public static void restore(final CardSprite card,
                               final List<ArmySprite> armies) {
        for (ArmySprite spr : armies) {
            if (spr.getArmy().getOwner().getClan()
                    .equals(card.getCard().getClan())) {
                spr.getArmy().setStrength(spr.getArmy().getStrength() + 1);
            }
        }
    }

    /**
     * Performs the Burn action on the CardSprite passed by the calling code.
     * This action finds the army with the corresponding clan and unit type
     * and reduces its strength by one. If that card is a Shrine, it reduces
     * the strength of all units of that Shrine's clan by one. This action has
     * no effect if the card burned does not belong to either Player (except
     * flipping the card on the UI).
     *
     * @param card the card being burned
     * @param armies a list of all living armies
     * @param activePlayer the active player (their lands will not be burned
     *                     while they are active)
     */
    public static void burn(final CardSprite card,
                            final List<ArmySprite> armies,
                            final Player activePlayer) {
        ArmySprite hurt = null;
        if (armies
                .stream().anyMatch(a -> a.getArmy().getType()
                        .equals(card.getCard().getPieceSupported())
                        && a.getArmy().getOwner().getClan()
                                .equals(card.getCard().getClan()))) {
            hurt = armies
                    .stream()
                    .filter(a -> a.getArmy().getType()
                            .equals(card.getCard().getPieceSupported())
                            && a.getArmy().getOwner().getClan()
                            .equals(card.getCard().getClan()))
                    .collect(Collectors.toList()).get(0);
        } else if (card.getCard().getPieceSupported().equals("Shrine")) {
            for (ArmySprite spr : armies) {
                if (spr.getArmy().getOwner().getClan()
                        .equals(card.getCard().getClan())) {
                    spr.getArmy().setStrength(spr.getArmy().getStrength() - 1);
                }
            }
        }
        if (hurt != null) {
            hurt.getArmy().setStrength(hurt.getArmy().getStrength() - 1);
        }
        if (activePlayer.getUser() != null) {
            activePlayer.getUser().setLandsBurned(
                    activePlayer.getUser().getLandsBurned() + 1);
        }
    }
}

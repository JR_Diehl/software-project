package awsutil;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;


/**
 * This class stores access credentials for the DynamoDB database that
 * will store users.
 *
 * @author JR Diehl
 * @version 1.0
 */
final class Credentials {
    /**
     * This is a static final instance of an AWSCredentialsProvider class
     * which holds the access key id and secret access key used to validate
     * access to the database.
     */
    public static final AWSCredentialsProvider CREDS =
            new AWSCredentialsProvider() {
        @Override
        public AWSCredentials getCredentials() {
            return new AWSCredentials() {
                @Override
                public String getAWSAccessKeyId() {
                    return "AKIAIWUGJ43IVV46KNHA";
                }

                @Override
                public String getAWSSecretKey() {
                    return "wQ+XmDlSd1u2HjN405xxxOADU5nuqK4DNE3YO3Dq";
                }
            };
        }

        @Override
        public void refresh() {

        }
    };

    /**
     * This is the hidden default constructor for this class. It is hidden to
     * prevent accidental or malicious instantiation of the utility class.
     */
    private Credentials() {

    }
}

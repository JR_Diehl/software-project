package awsutil;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;

import java.util.HashMap;
import java.util.Map;


/**
 * DBManager controls access to the Shoudo Database, which is implemented
 * through AWS DynamoDB. It allows only for the
 * {@link DBManager#getItem(String, String)} and
 * {@link DBManager#save(User, String)} methods, as table creation
 * and item and table deletion are not supported or needed by this application.
 *
 * @author JR Diehl
 * @version 1.0
 */
public final class DBManager {

    /**
     * AWS access credentials used to validate connection to the database.
     * These are obtained from the {@link Credentials} class.
     */
    private static final AWSCredentialsProvider CREDS = Credentials.CREDS;
    /**
     * DynamoDB client object, used to initialize {@link DBManager#DB} to
     * allow for table operations.
     */
    private static final AmazonDynamoDB CLIENT =
            AmazonDynamoDBAsyncClientBuilder.standard().
            withRegion(Regions.US_EAST_1).withCredentials(CREDS).build();
    /**
     * DynamoDB interface which controls CRUD operations on any tables which
     * can be accessed using the credentials provided in
     * {@link DBManager#CREDS}. In this application, only Create, Read, and
     * Update operations are used.
     */
    private static final DynamoDB DB = new DynamoDB(CLIENT);

    /**
     * Stores the name of the table being accessed, used across all methods.
     * Each method takes a name argument which is used to instantiate this if
     * it is null.
     */
    private static String name;

    /**
     * Default constructor for this class, hidden to prevent accidental or
     * malicious instantiation of this utility class.
     */
    private DBManager() {

    }

    /**
     * Gets the item with the specified username if it exists in the table at
     * {@link DBManager#name}. If name is null, tableName is used to
     * instantiate it; otherwise tableName is not used.
     *
     * @param username username to use as id to get from table
     * @param tableName table to search in if no table is already instantiated
     * @return User with desired username from database, or null if not found
     */
    public static User getItem(final String username, final String tableName) {
        if (name == null) {
            name = tableName;
        }
        if (username == null || username.equals("")) {
            return null;
        }

        Map<String, AttributeValue> keys =
                new HashMap<>();
        keys.put("Username", new AttributeValue().withS(username));

        GetItemRequest req = new GetItemRequest()
                .withKey(keys)
                .withTableName(name);

        GetItemResult result = CLIENT.getItem(req);
        Map<String, AttributeValue> item = result.getItem();

        if (item == null) {
            return null;
        }

        User u = new User(item.get("Username").getS(),
                item.get("Password").getS());

        u.setGamesWon(Integer.parseInt(item.get("GamesWon").getN()));
        u.setEmail(item.get("Email").getS());
        u.setConquestWins(Integer.parseInt(item.get("ConquestWins").getN()));
        u.setArmiesDestroyed(Integer.parseInt(
                item.get("ArmiesDestroyed").getN()));
        u.setGamesPlayedUesugi(Integer.parseInt(
                item.get("GamesPlayedUesugi").getN()));
        u.setGamesPlayedShimazu(Integer.parseInt(
                item.get("GamesPlayedShimazu").getN()));
        u.setGamesPlayedTokugawa(Integer.parseInt(
                item.get("GamesPlayedTokugawa").getN()));
        u.setRealName(item.get("RealName").getS());
        u.setGamesPlayedMori(Integer.parseInt(
                item.get("GamesPlayedMori").getN()));
        u.setLandsBurned(Integer.parseInt(item.get("LandsBurned").getN()));
        u.setGamesPlayed(Integer.parseInt(item.get("GamesPlayed").getN()));
        u.setSubjugationWins(Integer.parseInt(
                item.get("SubjugationWins").getN()));

        return u;
    }

    /**
     * Saves a {@link User} object to the current table (using tableName to
     * instantiate the table if it is null).
     *
     * @param u user to save
     * @param tableName table to instantiate if no table exists
     * @return Boolean whether save succeeded
     */
    public static Boolean save(final User u,
                               final String tableName) {
        Boolean notIdentical = true;

        if (name == null) {
            name = tableName;
        }

        if (getItem(u.getUsername(), tableName) != null) {
            notIdentical = false;
        }

        Item item = new Item()
                .withString("Email", u.getEmail())
                .withString("RealName", u.getRealName())
                .withPrimaryKey("Username", u.getUsername())
                .withInt("GamesWon", u.getGamesWon())
                .withInt("GamesPlayed", u.getGamesPlayed())
                .withInt("ArmiesDestroyed", u.getArmiesDestroyed())
                .withInt("LandsBurned", u.getLandsBurned())
                .withInt("SubjugationWins", u.getSubjugationWins())
                .withInt("ConquestWins", u.getConquestWins())
                .withInt("GamesPlayedUesugi", u.getGamesPlayedUesugi())
                .withInt("GamesPlayedMori", u.getGamesPlayedMori())
                .withInt("GamesPlayedTokugawa", u.getGamesPlayedTokugawa())
                .withInt("GamesPlayedShimazu", u.getGamesPlayedShimazu());
        if (notIdentical) {
            item = item.withString("Password", u.getEncryptedPassword());
        } else {
            item = item.withString("Password", u.getPassword());
        }

        Table table = DB.getTable(name);

        table.putItem(item);

        return notIdentical;
    }

    /**
     * Deletes the user from the table and returns true if successful. Returns
     * false otherwise.
     *
     * @param user user to be deleted
     * @param tableName table to delete from if not already initiated
     * @return Boolean whether delete was successful
     */
    public static Boolean deleteItem(final User user, final String tableName) {
        if (name == null) {
            name = tableName;
        }

        Table table = DB.getTable(tableName);

        DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
                .withPrimaryKey(new PrimaryKey("Username",
                        user.getUsername()));

        try {
            table.deleteItem(deleteItemSpec);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

}

package awsutil;

import org.jasypt.digest.config.SimpleStringDigesterConfig;
import org.jasypt.util.password.StrongPasswordEncryptor;
import java.util.Objects;


/**
 * This class is an information export for a registered system user.
 * It stores personal information and gameplay statistics, and enables
 * the system to check a provided password against the user's encrypted
 * password for validity.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class User {

    // Personal Info
    /**
     * Email registered for this user, if applicable. Can be empty.
     */
    private String email;
    /**
     * Full name registered for this user, if applicable. Can be empty.
     */
    private String realName;

    // Game Info
    /**
     * User's username, used as a primary key in the Shoudo Database.
     */
    private final String username;
    /**
     * Games won by this user over the history of their account. Includes
     * victories by opponent concession. Used to calculate win percentage.
     */
    private Integer gamesWon;
    /**
     * Games played by this user over the history of their account. Used to
     * calculate win percentage.
     */
    private Integer gamesPlayed;
    /**
     * Total armies destroyed in combat by this user over the history of their
     * account.
     */
    private Integer armiesDestroyed;
    /**
     * Total lands burned by this user over the history of their account.
     */
    private Integer landsBurned;
    /**
     * Total wins by subjugation (capturing opponent's Daimyo castle) by this
     * user over the history of their account. This is the default win for if
     * an opponent concedes.
     */
    private Integer subjugationWins;
    /**
     * Total wins by conquest (destroying all of opponent's armies) by this
     * user over the history of their account.
     */
    private Integer conquestWins;
    /**
     * Total games played as the Uesugi clan by this user over the history of
     * their account.
     */
    private Integer gamesPlayedUesugi;
    /**
     * Total games played as the Mori clan by this user over the history of
     * their account.
     */
    private Integer gamesPlayedMori;
    /**
     * Total games played as the Tokugawa clan by this user over the history of
     * their account.
     */
    private Integer gamesPlayedTokugawa;
    /**
     * Total games played as the Shimazu clan by this user over the history of
     * their account.
     */
    private Integer gamesPlayedShimazu;

    /**
     * Strong encryptor used to encrypt user's password for security in the
     * Shoudo database. Also used to verify correct password entered upon
     * attempted login.
     */
    private final StrongPasswordEncryptor crypt;
    /**
     * Configuration for password encryption. Used to ensure that password
     * encryption is predictable each time for correct password checking.
     */
    private final SimpleStringDigesterConfig config;

    // Password
    /**
     * User's password. Stored in plaintext in memory once when user first
     * registers; after this, it is always stored in an encrypted form.
     */
    private final String password;

    /**
     * Constructor for the User class. Initializes a new user with email and
     * real name empty, all gameplay statistics set to zero, and appropriate
     * objects for the {@link User#crypt} and {@link User#config} fields.
     * Username and password are set to values of uname and pword respectively.
     *
     * @param uname username for new user
     * @param pword password for new user
     */
    public User(final String uname, final String pword) {
        this.username = uname;
        this.password = pword;
        this.email = "";
        this.realName = "";
        this.gamesPlayedUesugi = 0;
        this.gamesPlayedMori = 0;
        this.gamesPlayedTokugawa = 0;
        this.gamesPlayedShimazu = 0;
        this.gamesPlayed = 0;
        this.gamesWon = 0;
        this.armiesDestroyed = 0;
        this.landsBurned = 0;
        this.conquestWins = 0;
        this.subjugationWins = 0;

        this.config = new SimpleStringDigesterConfig();
        config.setSaltSizeBytes(0);
        this.crypt = new StrongPasswordEncryptor();
    }

    /**
     * Checks pword against correct encrypted password. Used at login to
     * verify user's identity.
     *
     * @param pword password to check
     * @return Boolean whether password is correct or not
     */
    public final Boolean checkPassword(final String pword) {
        return this.crypt.checkPassword(pword, password);
    }

    /**
     * equals function for User class. Compares based on all fields except
     * password, crypt, and config.
     *
     * @param o object to compare against
     * @return boolean whether this object equals o
     */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(email, user.email)
                && Objects.equals(realName, user.realName)
                && Objects.equals(username, user.username)
                && Objects.equals(gamesWon, user.gamesWon)
                && Objects.equals(gamesPlayed, user.gamesPlayed)
                && Objects.equals(armiesDestroyed, user.armiesDestroyed)
                && Objects.equals(landsBurned, user.landsBurned)
                && Objects.equals(subjugationWins, user.subjugationWins)
                && Objects.equals(conquestWins, user.conquestWins)
                && Objects.equals(gamesPlayedUesugi, user.gamesPlayedUesugi)
                && Objects.equals(gamesPlayedMori, user.gamesPlayedMori)
                && Objects.equals(gamesPlayedTokugawa,
                user.gamesPlayedTokugawa)
                && Objects.equals(gamesPlayedShimazu, user.gamesPlayedShimazu);
    }

    /**
     * hashCode() function for User class. Implements standard Objects.hash()
     * behavior for all fields except password, crypt, and config.
     *
     * @return
     */
    @Override
    public final int hashCode() {

        return Objects.hash(email, realName, username, gamesWon, gamesPlayed,
                armiesDestroyed, landsBurned, subjugationWins, conquestWins,
                gamesPlayedUesugi, gamesPlayedMori, gamesPlayedTokugawa,
                gamesPlayedShimazu);
    }

    /**
     * @return {@link User#email}
     */
    public final String getEmail() {
        return email;
    }

    /**
     * @param e new value for email
     */
    public final void setEmail(final String e) {
        this.email = e;
    }

    /**
     * @return {@link User#realName}
     */
    public final String getRealName() {
        return realName;
    }

    /**
     * @param rn new value for realName
     */
    public final void setRealName(final String rn) {
        this.realName = rn;
    }

    /**
     * Gets the user's username. Important note: this field cannot be set
     * except in the constructor.
     *
     * @return {@link User#username}
     */
    public final String getUsername() {
        return username;
    }

    /**
     * Gets the user's password, likely in encrypted form. Important note:
     * this field cannot be set except in the constructor.
     *
     * @return {@link User#password}
     */
    public final String getPassword() {
        return this.password;
    }

    /**
     * Encrypts user's password before returning it. Used when first writing
     * plaintext password to Shoudo Database. Caution when using -- if an
     * already encrypted password is encrypted with this method, it will become
     * very difficult to retrieve!
     *
     * @return {@link User#password}, encrypted by {@link User#crypt}
     */
    public final String getEncryptedPassword() {
        return this.crypt.encryptPassword(password);
    }

    /**
     * @return {@link User#gamesWon}
     */
    public final Integer getGamesWon() {
        return gamesWon;
    }

    /**
     * Sets gamesWon to gw.
     *
     * @param gw new value for gamesWon
     */
    public final void setGamesWon(final Integer gw) {
        this.gamesWon = gw;
    }

    /**
     * @return {@link User#gamesPlayed}
     */
    public final Integer getGamesPlayed() {
        return gamesPlayed;
    }

    /**
     * Sets gamesPlayed to gp.
     *
     * @param gp new value for gamesPlayed
     */
    public final void setGamesPlayed(final Integer gp) {
        this.gamesPlayed = gp;
    }

    /**
     * Returns the name of user's most played clan. In case of tie,
     * ordering is Uesugi -> Mori -> Tokugawa -> Shimazu (initiative order)
     *
     * @return String most played clan
     */
    public final String getMostPlayedClan() {
        if (this.gamesPlayedUesugi.equals(this.gamesPlayedMori)
                && this.gamesPlayedUesugi.equals(this.gamesPlayedTokugawa)
                && this.gamesPlayedUesugi.equals(this.gamesPlayedShimazu)) {
            return "None";
        }
        int max = Integer.max(Integer.max(this.gamesPlayedUesugi,
                this.gamesPlayedMori),
                Integer.max(this.gamesPlayedTokugawa,
                        this.gamesPlayedShimazu));
        if (max == this.gamesPlayedUesugi) {
            return "Uesugi";
        }
        if (max == this.gamesPlayedMori) {
            return "Mori";
        }
        if (max == this.gamesPlayedTokugawa) {
            return "Tokugawa";
        }
        if (max == this.gamesPlayedShimazu) {
            return "Shimazu";
        }

        return null;
    }

    /**
     * @return {@link User#armiesDestroyed}
     */
    public final Integer getArmiesDestroyed() {
        return armiesDestroyed;
    }

    /**
     * Sets armiesDestroyed to ad.
     * @param ad new value for armiesDestroyed
     */
    public final void setArmiesDestroyed(final Integer ad) {
        this.armiesDestroyed = ad;
    }

    /**
     * @return {@link User#landsBurned}
     */
    public final Integer getLandsBurned() {
        return landsBurned;
    }

    /**
     * Sets landsBurned to lb.
     *
     * @param lb new value for landsBurned
     */
    public final void setLandsBurned(final Integer lb) {
        this.landsBurned = lb;
    }

    /**
     * @return {@link User#subjugationWins}
     */
    public final Integer getSubjugationWins() {
        return subjugationWins;
    }

    /**
     * Sets subjugationWins to sw.
     *
     * @param sw new value for subjugationWins
     */
    public final void setSubjugationWins(final Integer sw) {
        this.subjugationWins = sw;
    }

    /**
     * @return {@link User#conquestWins}
     */
    public final Integer getConquestWins() {
        return conquestWins;
    }

    /**
     * Sets conquestWins to cw.
     *
     * @param cw new value for conquestWins
     */
    public final void setConquestWins(final Integer cw) {
        this.conquestWins = cw;
    }

    /**
     * @return {@link User#gamesPlayedUesugi}
     */
    public final Integer getGamesPlayedUesugi() {
        return gamesPlayedUesugi;
    }

    /**
     * Sets gamesPlayedUesugi to gpu.
     *
     * @param gpu new value for gamesPlayedUesugi
     */
    public final void setGamesPlayedUesugi(final Integer gpu) {
        this.gamesPlayedUesugi = gpu;
    }

    /**
     * @return {@link User#gamesPlayedMori}
     */
    public final Integer getGamesPlayedMori() {
        return gamesPlayedMori;
    }

    /**
     * Sets gamesPlayedUesugi to gpm.
     *
     * @param gpm new value for gamesPlayedMori
     */
    public final void setGamesPlayedMori(final Integer gpm) {
        this.gamesPlayedMori = gpm;
    }

    /**
     * @return {@link User#getGamesPlayedTokugawa()}
     */
    public final Integer getGamesPlayedTokugawa() {
        return gamesPlayedTokugawa;
    }

    /**
     * Sets gamesPlayedUesugi to gpt.
     *
     * @param gpt new value for gamesPlayedTokugawaa
     */
    public final void setGamesPlayedTokugawa(final Integer gpt) {
        this.gamesPlayedTokugawa = gpt;
    }

    /**
     * @return {@link User#gamesPlayedShimazu}
     */
    public final Integer getGamesPlayedShimazu() {
        return gamesPlayedShimazu;
    }

    /**
     * Sets gamesPlayedUesugi to gps.
     *
     * @param gps new value for gamesPlayedShimazu
     */
    public final void setGamesPlayedShimazu(final Integer gps) {
        this.gamesPlayedShimazu = gps;
    }

    /**
     * Returns the encryptor used by the user. Used by
     * {@link DBManager#save(User, String, String)} to encrypt the user's
     * password on first time write to Shoudo Database.
     *
     * @return {@link User#crypt}
     */
    public final StrongPasswordEncryptor getCrypt() {
        return this.crypt;
    }
}

package ui;

import awsutil.DBManager;
import awsutil.User;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 * Main application menu for the Shoudo Application. This window allows the
 * user to launch a local game against an unregistered opponent, view account
 * details, and exit the game.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class Main extends Application {

    /**
     * The main window which will display content, including the main menu and
     * the local game when it is launched.
     */
    private Stage window;

    /**
     * Static final division factor used to scale the game logo.
     */
    private static final int IMAGEFACTOR = 4;
    /**
     * Static final spacing for Vbox.
     */
    private static final double VBOX = 30f;

    /**
     * Starts the main application by launching the primary window. This
     * acts as the entry point for everything in the application, including
     * launching the login window and managing the main menu.
     *
     * @param primaryStage the main window to display content to
     */
    @Override
    public final void start(final Stage primaryStage) {

        User user = LoginWindow.display();
        if (user == null) {
            return;
        }

        window = primaryStage;
        window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram(user);
        });

        int mainHeight = (int) Screen.getPrimary()
                .getVisualBounds().getHeight();
        int mainWidth = (int) Screen.getPrimary().getVisualBounds().getWidth();

        //Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        double xDim = Screen.getPrimary().getVisualBounds().getWidth();
        double yDim = Screen.getPrimary().getVisualBounds().getHeight();
        Rectangle2D bounds = new Rectangle2D((xDim - mainWidth) / 2,
                                            (yDim - mainHeight) / 2,
                mainWidth, mainHeight);

        window.setX(bounds.getMinX());
        window.setY(bounds.getMinY());
        window.setWidth(mainWidth);
        window.setHeight(mainHeight);

        // Local 2-player Game button
        Button localGameButton = new Button("New Local Game");
        localGameButton.setOnAction(e -> GameWindow.display(window, user));

        // Versus AI button
        Button versusAIButton = new Button("New Versus AI Game");
        versusAIButton.setOnAction(e -> AlertBox.display("Error!",
                "Sorry, this functionality is not supported in this release!"));

        // Load Versus AI button
        Button loadVersusAIButton = new Button("Load Versus AI Game");
        loadVersusAIButton.setOnAction(e -> AlertBox.display("Error!",
                "Sorry, this functionality is not supported in this release!"));

        // Account Details Button
        Button accountButton = new Button("Show Account Details");
        accountButton.setOnAction(e -> AccountDetailsWindow.display(user));

        // Close Button
        Button closeButton = new Button("Exit Shoudo");
        closeButton.setOnAction(e -> closeProgram(user));

        // Logo Image
        ImageView logo = new ImageView(new Image(Main.class.getResource(
                "/images/bad_logo.png").toExternalForm()));
        logo.setFitWidth((double) mainHeight / IMAGEFACTOR);
        logo.setFitHeight((double) mainHeight / IMAGEFACTOR);

        // Main Menu Button
        VBox mainMenuLayout = new VBox(VBOX);
        mainMenuLayout.getChildren().addAll(logo, localGameButton,
                versusAIButton, loadVersusAIButton, accountButton, closeButton);
        mainMenuLayout.setAlignment(Pos.CENTER);
        Scene mainMenu = new Scene(mainMenuLayout, mainWidth, mainHeight);
        mainMenu.getStylesheets().add(Main.class.getResource(
                "/styling/mainMenu1.css").toExternalForm());

        window.setTitle("Shoudo: the Scorched Earth Game");
        window.setScene(mainMenu);
        window.show();
    }

    /**
     * Controls exit from application, ensuring that if the user quits through
     * either the exit button or the close window option, their statistics
     * will be saved to the Shoudo Database.
     *
     * @param user user to save
     */
    private void closeProgram(final User user) {
        DBManager.save(user, "users");
        Boolean close = ConfirmBox.display("Really Quit?",
                "Are you sure you want to quit?");
        if (close) {
            window.close();
        }
    }

    /**
     * Main function; acts as a launcher for {@link Main#start(Stage)}.
     *
     * @param args command line arguments to main
     */
    public static void main(final String[] args) {
        launch(args);
    }
}

package ui;

import awsutil.DBManager;
import awsutil.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * Window that allows the user to enter a username and password which are
 * checked for validity. If they are valid, the user is logged in to the
 * Shoudo Database and can access the rest of the application. Also, a
 * user can open a {@link RegisterWindow} to register an account from this
 * window.
 *
 * @author JR Diehl
 * @version 1.0
 */
final class LoginWindow {

    /**
     * Internal user object used to track successful login and retrieve and
     * pass along user information on successful login.
     */
    private static User user = null;

    /**
     * Static final window width.
     */
    private static final double WIDTH = 400f;
    /**
     * Static final window height.
     */
    private static final double HEIGHT = 300f;
    /**
     * Static final gridpane insets.
     */
    private static final double INSETS = 10f;
    /**
     * Static final gridpane vertical gap.
     */
    private static final double VGAP = 8f;
    /**
     * Static final gridpane horizontal gap.
     */
    private static final double HGAP = 10f;

    /**
     * Column index for login button.
     */
    private static final int LOGINCOL = 2;
    /**
     * Column index for register button.
     */
    private static final int REGISTERCOL = 3;

    /**
     * Private constructor to prevent accidental or malicious initialization
     * of non-initializable window.
     */
    private LoginWindow() {

    }

    /**
     * Displays the login window, allowing the user to attempt to log in to
     * the Shoudo Application. If login is successful, the user will be
     * passed along to the main application. Otherwise, the program will
     * exit.
     *
     * @return user information if successful, null else
     */
    public static User display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Login");
        window.setMinWidth(WIDTH);
        window.setMinHeight(HEIGHT);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(INSETS, INSETS, INSETS, INSETS));
        grid.setVgap(VGAP);
        grid.setHgap(HGAP);

        Label userNameLabel = new Label("Username:");
        GridPane.setConstraints(userNameLabel, 0, 0);

        TextField userNameInput = new TextField();
        userNameInput.setPromptText("username here");
        GridPane.setConstraints(userNameInput, 1, 0);

        Label passwordLabel = new Label("Password:");
        GridPane.setConstraints(passwordLabel, 0, 1);

        PasswordField passwordInput = new PasswordField();
        passwordInput.setPromptText("password here");
        GridPane.setConstraints(passwordInput, 1, 1);

        Button loginButton = new Button("Login");
        loginButton.setOnAction(e -> {
            user = loginButtonClick(userNameInput.getText());
            if (user == null) {
                AlertBox.display("Incorrect Login!",
                        "Error: Username or Password was incorrect.");
                window.close();
            } else if (user.checkPassword(passwordInput.getText())) {
                window.close();
            } else {
                user = null;
                AlertBox.display("Incorrect Login!",
                        "Error: Username or Password was incorrect.");
                window.close();
            }
        });
        GridPane.setConstraints(loginButton, 1, LOGINCOL);

        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> {
            Boolean regSuccess = RegisterWindow.display();
            if (!regSuccess) {
                AlertBox.display("Register Failed!",
                        "Warning: Registration unsuccessful!");
            } else {
                AlertBox.display("Register Succeeded!",
                        "Registration successful!");
            }
        });
        GridPane.setConstraints(registerButton, 1, REGISTERCOL);

        grid.getChildren().addAll(userNameLabel, userNameInput,
                passwordLabel, passwordInput, loginButton, registerButton);
        grid.setAlignment(Pos.CENTER);

        Scene login = new Scene(grid, WIDTH, HEIGHT);
        login.getStylesheets().add(Main.class.getResource(
                "/styling/mainMenu1.css").toExternalForm());
        window.setScene(login);
        window.showAndWait();

        return user;
    }

    /**
     * Attempts to get a user item with the given username from the Shoudo
     * Database.
     *
     * @param username username to get from database
     * @return null if none, user with given username else
     */
    private static User loginButtonClick(final String username) {
        return DBManager.getItem(username, "users");
    }
}

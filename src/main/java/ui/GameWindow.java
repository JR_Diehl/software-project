package ui;

import awsutil.DBManager;
import awsutil.User;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

import shoudo.Actions;
import shoudo.Army;
import shoudo.Board;
import shoudo.Card;
import shoudo.GameRunner;
import shoudo.Player;
import shoudo.PlayerBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static shoudo.GameRunner.State.LOAD;
import static shoudo.GameRunner.State.PLAYER1_TURN;
import static shoudo.GameRunner.State.PLAYER2_TURN;
import static shoudo.GameRunner.State.SELECT;


/**
 * Controls the UI for a local game of Shoudo against an opponent. This class
 * also controls some of the game logic of running the game loop and state
 * machine as well.
 *
 * @author JR Diehl
 * @version 1.0
 */
final class GameWindow {

    /**
     * Store old scene so that it can be returned to after game is done.
     */
    private static Scene oldScene;

    /**
     * {@link GameRunner} instance to control the state machine regulating
     * player turns and other gameplay functionality.
     */
    private static GameRunner runner;
    /**
     * Width of game window.
     */
    private static double width;
    /**
     * Height of game window.
     */
    private static double height;
    /**
     * Currently active player. This is used to determine most conditionals,
     * as the active player is never targeting their own pieces and cards
     * with negative effects and actions.
     */
    private static Player activePlayer;

    /**
     * Static final value for rows. Ranges from 0 to 4.
     */
    private static final int ROWS = 5;
    /**
     * Static final value for cols. Ranges from 0 to 7.
     */
    private static final int COLS = 8;

    /**
     * List of living armies on the board. Used to detect movement blocks
     * and determine damage from burns. Also for movement of armies on
     * each player's turn.
     */
    private static List<ArmySprite> armies;
    /**
     * List of defeated armies. This is used to get strength scores for
     * castles whose armies have been defeated.
     */
    private static List<ArmySprite> vanquishedArmies;
    /**
     * List of CardSprites that is more easily accessible than a 2D list.
     * Used for searching through cards and filtering on cards.
     */
    private static List<CardSprite> cards;

    /**
     * List of selectable sprites available once an army has been selected.
     */
    private static List<Sprite> selectable;
    /**
     * ArmySprite that has been selected by the active player. Will always
     * belong to that player; a player cannot select their opponent's pieces.
     */
    private static ArmySprite selected;
    /**
     * CardSprite that the selected ArmySprite is standing on. Used for most
     * available action detection.
     */
    private static CardSprite selectedCard;

    /**
     * Tracker of remaining moves this turn. Decreases each time an army moves
     * during a turn.
     */
    private static int movesRemaining;
    /**
     * Static final number of moves allotted per turn. Currently set to three.
     */
    private static final int MOVES_PER_TURN = 3;

    /**
     * Text to display player 1 and username (if applicable).
     */
    private static Text player1Text;
    /**
     * Text to display player 2 and username (if applicable). Under current
     * design, the username for player 2 will always be "local player" since
     * player 2 always has a null user, but this implementation allows for
     * this to change in the future.
     */
    private static Text player2Text;
    /**
     * Text showing whose turn it is on player 1's menu.
     */
    private static Text currentTurnText1;
    /**
     * Text showing whose turn it is on player 2's menu.
     */
    private static Text currentTurnText2;
    /**
     * Text showing moves remaining this turn on player 1's menu.
     */
    private static Text movesRemainingText1;
    /**
     * Text showing moves remaining this turn on player 2's menu.
     */
    private static Text movesRemainingText2;
    /**
     * Text showing player 1's living army strengths. When an army is destroyed
     * its strength is no longer shown.
     */
    private static Text strengths1;
    /**
     * Text showing player 2's living army strengths. When an army is destroyed
     * its strength is no longer shown.
     */
    private static Text strengths2;
    /**
     * Image showing player 1's clan mon (emblem).
     */
    private static ImageView clanMon1;
    /**
     * Image showing player 2's clan mon (emblem).
     */
    private static ImageView clanMon2;

    /**
     * Current window which displays the game.
     */
    private static Stage thisWindow;
    /**
     * Canvas that graphics will be drawn to.
     */
    private static Canvas canvas;
    /**
     * GraphicsContext to control drawing of images.
     */
    private static GraphicsContext gc;

    /**
     * Static final spacing.
     */
    private static final double SPACING = 10f;
    /**
     * Static final scaling factor for canvas to ensure snug fit with both
     * player menus.
     */
    private static final double CANVASFACTOR = (6.8 / 10.0);
    /**
     * Static final offset factor that determines offset of board in x
     * dimension.
     */
    private static final double XOFFFACTOR = (8.0 / 8.8);
    /**
     * Static final offset factor that determines offset of board in y
     * dimension.
     */
    private static final double YOFFFACTOR = (5.0 / 5.2);
    /**
     * Static final divisor used when spacing cards on board in x dimension.
     */
    private static final double XDIV = 8.8;
    /**
     * Static final divisor used when spacing cards on board in y dimension.
     */
    private static final double YDIV = 5.2;
    /**
     * Static final x ratio used in computing dynamic card size.
     */
    private static final double CARDXRATIO = 10;
    /**
     * Static final y ratio used in computing dynamic card size.
     */
    private static final double CARDYRATIO = 6;
    /**
     * Static final number of clans available in game.
     */
    private static final int NUMCLANS = 4;
    /**
     * Static final width factor for mon.
     */
    private static final double MONWIDTH = 1.5 / 10.0;
    /**
     * Static final height factor for mon.
     */
    private static final double MONHEIGHT = 1.9 / 10.0;
    /**
     * Static final number of armies generated at start of game.
     */
    private static final int NUMPIECES = 6;
    /**
     * Static final default samurai army strength.
     */
    private static final int SAMURAISTRENGTH = 3;
    /**
     * Static final default heir army strength.
     */
    private static final int HEIRSTRENGTH = 4;
    /**
     * Static final default daimyo army strength.
     */
    private static final int DAIMYOSTRENGTH = 5;
    /**
     * Static final nanoseconds per second.
     */
    private static final double NANO = 1000000000.0;
    /**
     * Static final half second value.
     */
    private static final double HALFSEC = 0.5;

    /**
     * Private constructor to prevent accidental or malicious initialization
     * of non-initializable window.
     */
    private GameWindow() {

    }

    /**
     * Ends the current turn, resetting army actions taken and moves remaining.
     */
    private static void endTurn() {
        for (ArmySprite a:armies) {
            a.setActionTaken(false);
        }
        if (activePlayer.equals(runner.getPlayer1())) {
            runner.changeState(PLAYER2_TURN);
            activePlayer = runner.getPlayer2();
        } else {
            runner.changeState(PLAYER1_TURN);
            activePlayer = runner.getPlayer1();
        }

        updateCurrentPlayer();
        movesRemaining = MOVES_PER_TURN;
        updateRemainingMoves();
    }

    /**
     * Updates the current player Text fields.
     */
    private static void updateCurrentPlayer() {
        String currentTurn = "Player ";
        if (activePlayer.equals(runner.getPlayer1())) {
            currentTurn += "1's turn";
        } else if (activePlayer.equals(runner.getPlayer2())) {
            currentTurn += "2's turn";
        } else {
            currentTurn += "ERROR";
        }
        currentTurnText1.setText(currentTurn);
        currentTurnText1.setFill(Color.BLACK);
        currentTurnText2.setText(currentTurn);
        currentTurnText2.setFill(Color.BLACK);
    }

    /**
     * Updates the remainingMoves Text fields.
     */
    private static void updateRemainingMoves() {
        movesRemainingText1.setText("Moves Left: " + movesRemaining);
        movesRemainingText2.setText("Moves Left: " + movesRemaining);
    }

    /**
     * Gets string of a given army strength for a given player as a string.
     *
     * @param p player whose army is checked
     * @param army army being checked
     * @return string containing strength of army to be displayed in UI
     */
    private static String getArmyStrengthString(final Player p,
                                                final String army) {
        Army target = null;
        if (armies
                .stream().anyMatch(a -> a.getArmy().getOwner().equals(p)
                        && a.getArmy().getType().equals(army))
                || vanquishedArmies
                .stream().anyMatch(a -> a.getArmy().getOwner().equals(p)
                        && a.getArmy().getType().equals(army))) {
            target = armies
                    .stream()
                    .filter(a -> a.getArmy().getOwner().equals(p)
                            && a.getArmy().getType().equals(army))
                    .collect(Collectors.toList()).get(0).getArmy();
        }
        if (target != null) {
            return army + ": " + target.getStrength() + "\n";
        }
        return "";
    }

    /**
     * Updates the army strength Text fields for each player.
     */
    private static void updateStrengthTexts() {
        String str1 = "Army Strengths:\n";
        String str2 = "Army Strengths:\n";

        str1 += getArmyStrengthString(runner.getPlayer1(), "Daimyo");
        str1 += getArmyStrengthString(runner.getPlayer1(), "Heir");
        str1 += getArmyStrengthString(runner.getPlayer1(), "Samurai");

        str2 += getArmyStrengthString(runner.getPlayer2(), "Daimyo");
        str2 += getArmyStrengthString(runner.getPlayer2(), "Heir");
        str2 += getArmyStrengthString(runner.getPlayer2(), "Samurai");

        strengths1.setText(str1);
        strengths2.setText(str2);
    }

    /**
     * Draws the base game graphics (background, cards, and armies on board).
     *
     * @param elapsedTime time since last draw (for updating moving Sprites)
     */
    private static void drawGraphics(final double elapsedTime) {
        gc.restore();
        gc.drawImage(new Image(Main.class.getResource(
                "/images/game_bg.jpg").toExternalForm()),
                0, 0, width, height);
        for (CardSprite spr:cards) {
            spr.render(gc);
        }
        for (ArmySprite spr:armies) {
            spr.update(elapsedTime);
            spr.render(gc);
        }
    }

    /**
     * Updates user stats for the given player with the appropriate victory
     * type. Assumes that the player has a non-null user.
     *
     * @param player player being updated (has a non-null user)
     * @param winType type of win acheived
     */
    private static void updateUserStats(final Player player,
                                        final String winType) {
        if (winType.equals("Conquest")) {
            player.getUser().setConquestWins(
                    player.getUser().getConquestWins() + 1);
        } else {
            player.getUser().setSubjugationWins(
                    player.getUser().getSubjugationWins() + 1);
        }
        player.getUser().setGamesWon(
                player.getUser().getGamesWon() + 1);
        player.getUser().setGamesPlayed(
                player.getUser().getGamesPlayed() +  1);

        switch (player.getClan()) {
            case "Uesugi":
                player.getUser().setGamesPlayedUesugi(
                        player.getUser().getGamesPlayedUesugi() + 1);
                break;
            case "Mori":
                player.getUser().setGamesPlayedMori(
                        player.getUser().getGamesPlayedMori() + 1);
                break;
            case "Tokugawa":
                player.getUser().setGamesPlayedTokugawa(
                        player.getUser().getGamesPlayedTokugawa() + 1);
                break;
            default:
                player.getUser().setGamesPlayedShimazu(
                        player.getUser().getGamesPlayedShimazu() + 1);
                break;
        }

        DBManager.save(player.getUser(), "users");
    }

    /**
     * Quits the game, awarding one player the victory (of one of the two
     * types) and updating their user stats if possible.
     *
     * @param victory true if player 1 wins, false if player 2 wins
     * @param type type of victory
     */
    private static void quitGame(final Boolean victory, final String type) {

        Player winner;
        Player loser;

        if (victory) {
            winner = runner.getPlayer1();
            loser = runner.getPlayer2();
        } else {
            winner = runner.getPlayer2();
            loser = runner.getPlayer1();
        }

        if (winner.getUser() != null) {
            updateUserStats(winner, type);
        }
        if (loser.getUser() != null) {
            updateUserStats(loser, type);
        }

    }

    /**
     * Initializes the game environment by setting fields to valid values, then
     * loading data and drawing the starting board and selecting starting
     * player.
     */
    private static void initGame() {
        runner = GameRunner.getInstance();
        oldScene = thisWindow.getScene();
        VBox p1Controls = new VBox();
        p1Controls.setAlignment(Pos.CENTER);
        p1Controls.setSpacing(SPACING);
        p1Controls.getStylesheets().addAll(Main.class.getResource(
                "/styling/p1Controls1.css").toExternalForm());

        VBox p2Controls = new VBox();
        p2Controls.setAlignment(Pos.CENTER);
        p2Controls.setSpacing(SPACING);
        p2Controls.getStylesheets().addAll(Main.class.getResource(
                "/styling/p2Controls1.css").toExternalForm());

        player1Text = new Text("Player 1: local player");
        player1Text.setId("defaultText");
        player2Text = new Text("Player 2: local player");
        player2Text.setId("defaultText");
        clanMon1 = new ImageView();
        clanMon2 = new ImageView();
        currentTurnText1 = new Text();
        currentTurnText1.setId("defaultText");
        currentTurnText2 = new Text();
        currentTurnText2.setId("defaultText");
        movesRemainingText1 = new Text();
        movesRemainingText1.setId("defaultText");
        movesRemainingText2 = new Text();
        movesRemainingText2.setId("defaultText");
        strengths1 = new Text();
        strengths1.setId("defaultText");
        strengths2 = new Text();
        strengths2.setId("defaultText");
        Button endTurnButton1 = new Button("End Turn");
        endTurnButton1.setOnAction(e -> {
            if (activePlayer.equals(runner.getPlayer1())) {
                endTurn();
            }
        });
        Button endTurnButton2 = new Button("End Turn");
        endTurnButton2.setOnAction(e -> {
            if (activePlayer.equals(runner.getPlayer2())) {
                endTurn();
            }
        });

        BorderPane root = new BorderPane();
        Scene game = new Scene(root);
        thisWindow.setScene(game);

        game.getStylesheets().add(Main.class.getResource(
                "/styling/gameWindow1.css").toExternalForm());

        width = Screen.getPrimary().getVisualBounds().getWidth();
        height = Screen.getPrimary().getVisualBounds().getHeight();
        armies = new ArrayList<>();
        vanquishedArmies = new ArrayList<>();
        cards = new ArrayList<>();
        selectable = new ArrayList<>();

        canvas = new Canvas((int) width * CANVASFACTOR, (int) height);

        gc = canvas.getGraphicsContext2D();

        // Quit game button 1
        Button quitGameButton1 = new Button("Quit Game");
        quitGameButton1.setOnAction(e -> {
            quitGame(false, "");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        });

        // Quit game button 2
        Button quitGameButton2 = new Button("Quit Game");
        quitGameButton2.setOnAction(e -> {
            quitGame(true, "");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        });

        p1Controls.getChildren().addAll(player1Text,
                clanMon1, currentTurnText1,
                movesRemainingText1, strengths1,
                endTurnButton1, quitGameButton1);
        p2Controls.getChildren().addAll(player2Text,
                clanMon2, currentTurnText2,
                movesRemainingText2, strengths2,
                endTurnButton2, quitGameButton2);
        root.setLeft(p1Controls);
        root.setCenter(canvas);
        root.setRight(p2Controls);
    }

    /**
     * Creates a cardsprite using the given values as field initializations.
     *
     * @param card internal card object
     * @param xOffset x offset to compute x position (with c)
     * @param yOffset y offset to compute y position (with r)
     * @param r row to draw card in
     * @param c column to draw card in
     * @return new CardSprite object at specified location with given internal
     * card
     */
    private static CardSprite createCard(final Card card,
                                         final double xOffset,
                                         final double yOffset,
                                         final int r,
                                         final int c) {
        CardSprite csprite = new CardSprite();
        csprite.setCard(card);
        csprite.setUnburnedImage(new Image(card.getImage()));
        csprite.setBurnedImage(new Image(card.getBurnedImage()));
        csprite.setImage(csprite.getUnburnedImage());
        csprite.setPosX(xOffset + c * canvas.getWidth() / XDIV);
        csprite.setPosY(yOffset + r * canvas.getHeight() / YDIV);
        csprite.setVelX(0);
        csprite.setVelY(0);
        csprite.setRow(r);
        csprite.setCol(c);
        csprite.setHeight(canvas.getHeight() / CARDYRATIO);
        csprite.setWidth(canvas.getWidth() / CARDXRATIO);
        csprite.render(gc);
        return csprite;
    }

    /**
     * Initializes mon graphics for both players.
     */
    private static void initMon() {
        String monPath = "/images/mon"
                + runner.getPlayer1().getInitiative() + ".png";
        clanMon1.setImage(new Image(
                Main.class.getResource(monPath).toExternalForm(),
                width * MONWIDTH,
                width * MONHEIGHT,
                true, true));
        monPath = "/images/mon"
                + runner.getPlayer2().getInitiative() + ".png";
        clanMon2.setImage(new Image(
                Main.class.getResource(monPath).toExternalForm(),
                width * MONWIDTH,
                width * MONHEIGHT,
                true, true));
    }

    /**
     * Initializes an army for the given ArmySprite, using i to determine
     * which player and type.
     *
     * @param asprite ArmySprite to initialize internal Army for
     * @param i controller value
     */
    private static void initArmy(final ArmySprite asprite, final int i) {
        CardSprite cs;
        if (i < 2) {
            cs = cards
                    .stream()
                    .filter(x -> x.getCard().getIsCastle()
                            && x.getCard().getPieceSupported()
                            .equals("Samurai")
                            && x.getCard().getClan()
                            .equals(asprite.getArmy()
                                    .getOwner().getClan()))
                    .collect(Collectors.toList())
                    .get(0);
        } else if (i < NUMCLANS) {
            cs = cards
                    .stream()
                    .filter(x -> x.getCard().getIsCastle()
                            && x.getCard().getPieceSupported()
                            .equals("Heir")
                            && x.getCard().getClan()
                            .equals(asprite.getArmy()
                                    .getOwner().getClan()))
                    .collect(Collectors.toList())
                    .get(0);
        } else {
            cs = cards
                    .stream()
                    .filter(x -> x.getCard().getIsCastle()
                            && x.getCard().getPieceSupported()
                            .equals("Daimyo")
                            && x.getCard().getClan()
                            .equals(asprite.getArmy()
                                    .getOwner().getClan()))
                    .collect(Collectors.toList())
                    .get(0);
        }

        asprite.setPosX((cs.getWidth() - asprite.getImage().getWidth())
                / 2 + cs.getPosX());
        asprite.setPosY((cs.getHeight() - asprite.getImage().getHeight())
                / 2 + cs.getPosY());
        asprite.setWidth(asprite.getImage().getWidth());
        asprite.setHeight(asprite.getImage().getHeight());
        asprite.setVelX(0);
        asprite.setVelY(0);

        asprite.getArmy().setCol(cs.getCol());
        asprite.getArmy().setRow(cs.getRow());
        if (i < 2) {
            asprite.getArmy().setStrength(SAMURAISTRENGTH);
            asprite.getArmy().setType("Samurai");
        } else if (i < NUMCLANS) {
            asprite.getArmy().setStrength(HEIRSTRENGTH);
            asprite.getArmy().setType("Heir");
        } else {
            asprite.getArmy().setStrength(DAIMYOSTRENGTH);
            asprite.getArmy().setType("Daimyo");
        }
        asprite.render(gc);
        armies.add(asprite);
        runner.getArmies().add(asprite.getArmy());
    }

    /**
     * Initializes the game board, assigning the active user as the internal
     * user of player 1.
     *
     * @param active active user, assigned to player 1 internal
     */
    private static void initBoard(final User active) {
        Board board = runner.generateNewBoard();

        double xOffset = (canvas.getWidth() - canvas.getWidth()
                * XOFFFACTOR) / 2.0;
        double yOffset = (canvas.getHeight() - canvas.getHeight()
                * YOFFFACTOR) / 2.0;

        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                Card card = board.getBoard().get(r).get(c);
                CardSprite csprite = createCard(card,
                        xOffset,
                        yOffset,
                        r, c);
                cards.add(csprite);
            }
        }

        Random rand = new Random();

        // builder
        PlayerBuilder playerBuilder = new PlayerBuilder();

        Player p1 = playerBuilder
                .buildDefaultPlayer()
                .withRandomInitiative()
                .withUser(active).getPlayer();
        activePlayer = p1;
        Player p2 = playerBuilder
                .buildDefaultPlayer()
                .withRandomInitiative()
                .withUser(null).getPlayer();
        while (p2.getInitiative().equals(p1.getInitiative())) {
            int init = rand.nextInt() % NUMCLANS;
            if (init < 0) {
                init = init * -1;
            }
            init++;
            p2.setInitiative(init);
        }

        if (p2.getInitiative() < p1.getInitiative()) {
            activePlayer = p2;
        }

        runner.setPlayer1(p1);
        runner.setPlayer2(p2);

        if (runner.getPlayer1().getUser() != null) {
            player1Text.setText("Player 1: "
                    + runner.getPlayer1().getUser().getUsername());
        }
        if (runner.getPlayer2().getUser() != null) {
            player2Text.setText("Player 2: "
                    + runner.getPlayer2().getUser().getUsername());
        }

        initMon();

        for (int i = 0; i < NUMPIECES; i++) {
            ArmySprite asprite = new ArmySprite();
            String imgPath;
            if (i < 2) {
                imgPath = "/images/pieces/SwordMeeple";
            } else if (i < NUMCLANS) {
                imgPath = "/images/pieces/FanMeeple";
            } else {
                imgPath = "/images/pieces/KabutoMeeple";
            }
            asprite.setArmy(new Army());
            if (i % 2 == 0) {
                asprite.getArmy().setOwner(p1);
                imgPath += p1.getInitiative();
            } else {
                asprite.getArmy().setOwner(p2);
                imgPath += p2.getInitiative();
            }
            imgPath += ".png";

            asprite.setImage(new Image(
                    Main.class.getResource(imgPath).toExternalForm()));

            initArmy(asprite, i);

            if (activePlayer.equals(runner.getPlayer1())) {
                runner.changeState(PLAYER1_TURN);
            } else {
                runner.changeState(PLAYER2_TURN);
            }
            updateCurrentPlayer();
            movesRemaining = MOVES_PER_TURN;
            updateStrengthTexts();
            updateRemainingMoves();
        }
    }

    /**
     * Destroys an army or castle after combat.
     *
     * @param destroy army to be destroyed
     * @param attacker attacker from combat
     * @param defender defender from combat
     * @param defenderIsCastle whether defender was a castle
     */
    private static void handleDestroy(final ArmySprite destroy,
                                      final ArmySprite attacker,
                                      final ArmySprite defender,
                                      final Boolean defenderIsCastle) {
        if (destroy != null) {
            if (armies.contains(destroy)) {
                armies.remove(destroy);
                vanquishedArmies.add(destroy);
                if (defenderIsCastle && destroy.equals(defender)
                        && attacker.getArmy().getOwner().getUser() != null) {
                    attacker.getArmy().getOwner().getUser().setLandsBurned(
                            attacker.getArmy().getOwner()
                                    .getUser().getLandsBurned() + 1);
                } else if (destroy.equals(attacker)
                        && defender.getArmy().getOwner().getUser() != null) {
                    defender.getArmy().getOwner().getUser().setArmiesDestroyed(
                            defender.getArmy().getOwner()
                                    .getUser().getArmiesDestroyed() + 1);
                } else if (attacker.getArmy().getOwner().getUser() != null) {
                    attacker.getArmy().getOwner().getUser().setArmiesDestroyed(
                            attacker.getArmy().getOwner()
                                    .getUser().getArmiesDestroyed() + 1);
                }
            }
        }
    }

    /**
     * Checks if daimyo castle is burned for the given player.
     *
     * @param p player to check
     * @return boolean whether the player's daimyo castle is burned
     */
    private static boolean checkIfDaimyoCastleBurned(final Player p) {
        return cards
                .stream()
                .filter(c -> c.getImage().equals(c.getBurnedImage())
                        && c.getCard().getIsCastle()
                        && c.getCard().getPieceSupported().equals("Daimyo")
                        && c.getCard().getClan().equals(p.getClan()))
                .count() == 1;
    }

    /**
     * Checks if the game has ended.
     */
    private static void checkIfGameOver() {
        if (armies
                .stream().noneMatch(a -> a.getArmy()
                        .getOwner().equals(runner.getPlayer1()))) {
            quitGame(false, "Conquest");
            AlertBox.display("Game Over!",
                    "Player 2 wins by Conquest!");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        } else if (armies
                .stream().noneMatch(a -> a.getArmy()
                        .getOwner().equals(runner.getPlayer2()))) {
            quitGame(true, "Conquest");
            AlertBox.display("Game Over!",
                    "Player 1 wins by Conquest!");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        } else if (checkIfDaimyoCastleBurned(runner.getPlayer1())) {
            quitGame(false, "Subjugation");
            AlertBox.display("Game Over!",
                    "Player 2 wins by Subjugation!");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        } else if (checkIfDaimyoCastleBurned(runner.getPlayer2())) {
            quitGame(true, "Subjugation");
            AlertBox.display("Game Over!",
                    "Player 1 wins by Subjugation!");
            GameRunner.killInstance();
            thisWindow.setScene(oldScene);
        }
    }

    /**
     * Gets the CardSprite beneath the ArmySprite that was clicked, or null if
     * no ArmySprite was clicked.
     *
     * @param click location of click
     * @return CardSprite of clicked ArmySprite, or null if fail
     */
    private static CardSprite getClicked(final Point2D click) {
        CardSprite clicked = null;

        for (Sprite spr : selectable) {
            CardSprite cardSprite = (CardSprite) spr;
            cardSprite.setOverlayMode("off");
        }

        if (selectable
                .stream().filter(x -> x.getBoundary().contains(click))
                .collect(Collectors.toList()).size() == 0) {
            runner.changeState(runner.getLastState());
        } else {
            clicked = (CardSprite) selectable
                    .stream()
                    .filter(x -> x.getBoundary()
                            .contains(click))
                    .collect(Collectors.toList()).get(0);
        }

        return clicked;
    }

    /**
     * Determines if the clicked card can be attacked.
     *
     * @param clicked CardSprite clicked
     * @return boolean whether valid attack exists
     */
    private static boolean canAttack(final CardSprite clicked) {
        return !selected.getActionTaken()
                && armies
                .stream().anyMatch(a ->
                        a.getArmy().getRow() == clicked.getRow()
                                && a.getArmy().getCol() == clicked.getCol()
                                && !a.getArmy().getOwner().getClan()
                                .equals(activePlayer.getClan()))
                || (!clicked.getCard().getClan()
                .equals(activePlayer.getClan())
                && clicked.getCard().getIsCastle()
                && clicked.getImage()
                .equals(clicked.getUnburnedImage()));
    }

    /**
     * Determines if the clicked card can be burned.
     *
     * @param clicked CardSprite clicked
     * @return boolean whether valid burn exists
     */
    private static boolean canBurn(final CardSprite clicked) {
        return !selected.getActionTaken()
                && selected.getArmy().getRow() == clicked.getRow()
                && selected.getArmy().getCol() == clicked.getCol()
                && selectedCard.getCard().getPieceSupported()
                .equals("Shrine")
                && selectedCard.getCard().getClan()
                .equals(activePlayer.getClan());
    }

    /**
     * Determines if the clicked card can be burned.
     *
     * @param clicked CardSprite clicked
     * @return boolean whether valid restore exists
     */
    private static boolean canRestore(final CardSprite clicked) {
        return !selected.getActionTaken()
                && selected.getArmy().getRow() == clicked.getRow()
                && selected.getArmy().getCol() == clicked.getCol()
                && !selectedCard.getCard().getClan()
                .equals(activePlayer.getClan())
                && !selectedCard.getCard().getImage()
                .equals(selectedCard.getCard().getBurnedImage());
    }

    /**
     * Determines if the clicked card can be moved to.
     *
     * @param clicked CardSprite clicked
     * @return boolean whether valid move exists
     */
    private static boolean canMove(final CardSprite clicked) {
        return movesRemaining > 0 && armies
                .stream().noneMatch(a -> a.getArmy()
                        .getRow() == clicked.getRow()
                        && a.getArmy().getCol()
                        == clicked.getCol()
                        && a.getArmy().getOwner()
                        .getClan().equals(activePlayer.getClan()));
    }

    /**
     * EventHandler for player turn. Allows active player to select own armies,
     * which transitions to {@link GameWindow#selectHandler}.
     */
    private static EventHandler<MouseEvent> turnHandler
            = new EventHandler<MouseEvent>() {
        @Override
        public void handle(final MouseEvent event) {
            double x = event.getX();
            double y = event.getY();

            Rectangle2D bound;
            ArmySprite clicked = null;
            for (ArmySprite a : armies) {
                bound = a.getBoundary();
                if (bound.contains(x, y)) {
                    clicked = a;
                    break;
                }
            }

            if (clicked == null) {
                return;
            }

            if (!clicked.getArmy().getOwner()
                    .equals(activePlayer)) {
                return;
            }

            // iterator
            Iterator<CardSprite> it = cards.iterator();
            CardSprite cs;
            while (it.hasNext()) {
                cs = it.next();
                if (cs.getCol() == clicked.getArmy().getCol()
                        && cs.getRow() == clicked
                        .getArmy().getRow()) {
                    selectedCard = cs;
                    break;
                }
            }

            selected = clicked;

            List<CardSprite> moves = selectedCard
                    .getValidMoves(cards, armies,
                            activePlayer.getClan());

            for (CardSprite spr : moves) {
                spr.setOverlayMode("move");
            }

            List<CardSprite> actions = selectedCard
                    .getActions(cards, armies,
                            activePlayer.getClan());

            for (CardSprite spr : actions) {
                if (canAttack(spr)) {
                    spr.setOverlayMode("attack");
                } else {
                    spr.setOverlayMode("restore");
                }
            }

            selectable.clear();

            selectable.addAll(moves);
            selectable.addAll(actions);

            runner.changeState(SELECT);
        }
    };

    /**
     * EventHandler for selection resolution. Allows active player to choose
     * a valid move or action to execute, or to cancel by clicking elsewhere.
     */
    private static EventHandler<MouseEvent> selectHandler
            = new EventHandler<MouseEvent>() {
        @Override
        public void handle(final MouseEvent event) {
            Point2D click = new Point2D(event.getX(), event.getY());

            CardSprite clicked = getClicked(click);

            // Attack
            if (canAttack(clicked)) {
                selected.setActionTaken(true);
                Card attCard = selectedCard.getCard();
                Card defCard = clicked.getCard();
                ArmySprite attacker = selected;
                Boolean defenderIsCastle = false;
                Boolean defenderIsNeutral = false;
                List<ArmySprite> defendingArmies = armies
                        .stream()
                        .filter(a -> a.getArmy()
                                .getCol() == clicked
                                .getCol()
                                && a.getArmy().getRow()
                                == clicked.getRow())
                        .collect(Collectors.toList());
                ArmySprite defender;
                if (defendingArmies.size() > 0) {
                    defender = defendingArmies.get(0);
                } else {
                    defenderIsCastle = true;
                    if (armies
                            .stream().anyMatch(a ->
                            a.getArmy().getType().equals(clicked.getCard()
                                            .getPieceSupported()))) {
                        defender = armies
                                .stream()
                                .filter(a ->
                                a.getArmy().getType()
                                .equals(clicked.getCard()
                                        .getPieceSupported()))
                                .collect(Collectors.toList()).get(0);
                    } else {
                        defender = vanquishedArmies
                                .stream()
                                .filter(a ->
                                a.getArmy().getType()
                                .equals(clicked.getCard()
                                .getPieceSupported()))
                                .collect(Collectors.toList()).get(0);
                    }
                }
                if (defenderIsCastle && !clicked.getCard().getClan()
                        .equals(defender.getArmy().getOwner().getClan())) {
                    defenderIsNeutral = true;
                }

                ArmySprite destroy = null;

                Army won = Actions.attack(attacker.getArmy(),
                        defender.getArmy(),
                        attCard, defCard,
                        defenderIsNeutral, movesRemaining > 0);

                Random rand = new Random();
                if (won == null) {
                    List<CardSprite> sprites = selectedCard
                            .getValidMoves(cards, armies,
                                    attacker.getArmy()
                                            .getOwner().getClan());
                    int i = rand.nextInt() % sprites.size();
                    if (i < 0) {
                        i = i * -1;
                    }
                    CardSprite dest = sprites.get(i);
                    attacker.move((dest.getWidth()
                                    - attacker.getImage()
                                    .getWidth()) / 2 + dest.getPosX(),
                            (dest.getHeight()
                                    - attacker.getImage()
                                    .getHeight()) / 2 + dest.getPosY(),
                            HALFSEC);
                    attacker.getArmy().setCol(dest.getCol());
                    attacker.getArmy().setRow(dest.getRow());
                    movesRemaining--;
                    updateRemainingMoves();
                } else if (won.equals(attacker.getArmy())) {
                    if (defenderIsCastle) {
                        clicked.flip(gc);
                    }
                    if (!defenderIsNeutral) {
                        destroy = defender;
                    }
                } else if (won.equals(defender.getArmy())) {
                    destroy = attacker;
                }

                handleDestroy(destroy, attacker, defender,
                        defenderIsCastle);

                checkIfGameOver();
            } else if (canBurn(clicked)) {
                selected.setActionTaken(true);
                selectedCard.flip(gc);
                Actions.restore(selectedCard, armies);
                updateStrengthTexts();
            } else if (canRestore(clicked)) {
                selected.setActionTaken(true);
                selectedCard.flip(gc);
                Actions.burn(selectedCard, armies, activePlayer);
                updateStrengthTexts();
            } else if (canMove(clicked)) {
                selected.move((clicked.getWidth()
                                - selected.getImage()
                                .getWidth()) / 2 + clicked.getPosX(),
                        (clicked.getHeight()
                                - selected.getImage()
                                .getHeight()) / 2 + clicked.getPosY(),
                        HALFSEC);
                selected.getArmy().setCol(clicked.getCol());
                selected.getArmy().setRow(clicked.getRow());
                movesRemaining--;
                updateRemainingMoves();
            }
            selected = null;
            selectedCard = null;
            selectable.clear();
            if (movesRemaining == 0
                    && armies
                    .stream().noneMatch(a -> a
                            .getArmy().getOwner().equals(activePlayer)
                            && !a.getActionTaken())) {
                endTurn();
            }
            runner.changeState(runner.getLastState());
        }
    };

    /**
     * Displays the game window and runs the game in a loop until one player
     * wins or quits.
     *
     * @param window game window
     * @param active active user to load into player 1
     */
    public static void display(final Stage window, final User active) {
        thisWindow = window;
        initGame();

        new AnimationTimer() {
            private double lastNanoTime;
            @Override
            public void handle(final long now) {
                // calculate time since last update.
                double elapsedTime = (now - lastNanoTime) / NANO;
                this.lastNanoTime = now;

                if (runner.getGameState() != LOAD) {
                    drawGraphics(elapsedTime);
                }

                switch (runner.getGameState()) {
                    case LOAD:
                        initBoard(active);
                        break;
                    case PLAYER1_TURN:
                    case PLAYER2_TURN:
                        canvas.setOnMouseClicked(turnHandler);
                        break;
                    case SELECT:
                        canvas.setOnMouseClicked(selectHandler);
                        break;
                    default:
                        break;
                }
            }
        }.start();
    }

}

package ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Displays an alert containing some text to notify the user of an event or
 * system warning.
 *
 * @author JR Diehl
 * @version 1.0
 */
public final class AlertBox {

    /**
     * Static final value of window width.
     */
    private static final double WIDTH = 300f;
    /**
     * Static final value of window height.
     */
    private static final double HEIGHT = 300f;
    /**
     * Static final spacing value for a Vbox.
     */
    private static final double VBOX = 10f;

    /**
     * Private constructor to prevent accidental or malicious initialization
     * of non-initializable window.
     */
    private AlertBox() {

    }

    /**
     * Displays the alert box with window title equal to title containing a
     * the message passed from the calling code.
     *
     * @param title title for alert window
     * @param message message for alert window
     */
    public static void display(final String title, final String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(WIDTH);
        window.setMinHeight(HEIGHT);

        Label label = new Label();
        label.setText(message);

        VBox layout = new VBox(VBOX);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}

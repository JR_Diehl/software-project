package ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Opens a box with a title and message, as well as a prompt that the user can
 * either accept by clicking the yesButton or reject by clicking the noButton.
 *
 * @author JR Diehl
 * @version 1.0
 */
public final class ConfirmBox {

    /**
     * The user's response to the prompt.
     */
    private static Boolean resp;

    /**
     * Static final window width.
     */
    private static final double WIDTH = 300f;
    /**
     * Static final window height.
     */
    private static final double HEIGHT = 300f;
    /**
     * Static final Vbox spacing.
     */
    private static final double VBOX = 10f;
    /**
     * Static final Hbox spacing.
     */
    private static final double HBOX = 10f;

    /**
     * Private constructor to prevent accidental or malicious initialization
     * of non-initializable window.
     */
    private ConfirmBox() {

    }

    /**
     * Displays the confirm box with window title equal to title containing a
     * the message passed from the calling code, and yes and no buttons.
     *
     * @param title title for confirm window
     * @param message message for confirm window
     * @return Boolean whether the user selected yes or no
     */
    public static Boolean display(final String title, final String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(WIDTH);
        window.setMinHeight(HEIGHT);

        Label label = new Label();
        label.setText(message);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");
        yesButton.setOnAction(e -> {
            resp = true;
            window.close();
        });
        noButton.setOnAction(e -> {
            resp = false;
            window.close();
        });

        VBox layout = new VBox(VBOX);
        HBox buttons = new HBox(HBOX);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(yesButton, noButton);
        layout.getChildren().addAll(label, buttons);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return resp;
    }
}

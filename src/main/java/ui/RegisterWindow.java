package ui;

import awsutil.DBManager;
import awsutil.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * Allows a potential user to register an account to the Shoudo Database,
 * allowing them to log in to the system and access the rest of the
 * application.
 *
 * @author JR Diehl
 * @version 1.0
 */
final class RegisterWindow {

    /**
     * Whether registrataion was successful.
     */
    private static Boolean success = false;

    /**
     * Static final window width.
     */
    private static final double WIDTH = 400f;
    /**
     * Static final window height.
     */
    private static final double HEIGHT = 300f;
    /**
     * Static final gridpane insets.
     */
    private static final double INSETS = 10f;

    /**
     * Static final horizontal gap for gridpane.
     */
    private static final double HGAP = 10f;
    /**
     * Static final vertical gap for gridpane.
     */
    private static final double VGAP = 8f;

    /**
     * Static final value of 0 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL0 = 0;
    /**
     * Static final value of 1 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL1 = 1;
    /**
     * Static final value of 2 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL2 = 2;
    /**
     * Static final value of 3 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL3 = 3;
    /**
     * Static final value of 4 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL4 = 4;

    /**
     * Private constructor to prevent accidental or malicious initialization
     * of non-initializable window.
     */
    private RegisterWindow() {

    }

    /**
     * Displays the register window, waiting for the user to enter a username
     * and password (and, optionally, a real name and/or email), then attempts
     * to register the user with the given credentials.
     *
     * @return Boolean whether registration was successful
     */
    public static Boolean display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Register new user");
        window.setMinWidth(WIDTH);
        window.setMinHeight(HEIGHT);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(INSETS, INSETS, INSETS, INSETS));
        grid.setHgap(HGAP);
        grid.setVgap(VGAP);

        // Labels
        Label userNameLabel = new Label("Username:");
        Label passwordLabel = new Label("Password:");
        Label realNameLabel = new Label("Real Name:");
        Label emailLabel = new Label("Email:");

        // Fields
        TextField userNameField = new TextField();
        userNameField.setPromptText("(required)");
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("(required)");
        TextField realNameField = new TextField();
        realNameField.setPromptText("(optional)");
        TextField emailField = new TextField();
        emailField.setPromptText("(optional)");

        // Buttons
        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> {
            User u = new User(userNameField.getText(),
                    passwordField.getText());
            if (realNameField.getText().length() == 0) {
                u.setRealName("n/a");
            } else {
                u.setRealName(realNameField.getText());
            }
            if (emailField.getText().length() == 0) {
                u.setEmail("n/a");
            } else {
                u.setEmail(emailField.getText());
            }
            success = DBManager.save(u, "users");
            window.close();
        });

        // Load Labels/Fields into grid
        GridPane.setConstraints(userNameLabel, VAL0, VAL0);
        GridPane.setConstraints(passwordLabel, VAL0, VAL1);
        GridPane.setConstraints(realNameLabel, VAL0, VAL2);
        GridPane.setConstraints(emailLabel, VAL0, VAL3);
        GridPane.setConstraints(userNameField, VAL1, VAL0);
        GridPane.setConstraints(passwordField, VAL1, VAL1);
        GridPane.setConstraints(realNameField, VAL1, VAL2);
        GridPane.setConstraints(emailField, VAL1, VAL3);
        GridPane.setConstraints(registerButton, VAL1, VAL4);

        grid.getChildren().addAll(userNameField, passwordField,
                realNameField, emailField, userNameLabel, passwordLabel,
                realNameLabel, emailLabel, registerButton);
        grid.setAlignment(Pos.CENTER);

        Scene register = new Scene(grid, WIDTH, HEIGHT);
        register.getStylesheets().add(Main.class.getResource(
                "/styling/mainMenu1.css").toExternalForm());
        window.setScene(register);
        window.showAndWait();

        return success;
    }

}

package ui;

import awsutil.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * @author JR Diehl
 */
final class AccountDetailsWindow {

    /**
     * User whose details are being retrieved.
     */
    private static User user;

    // Personal Info (maybe unneeded)
    /**
     * User's email (can be empty).
     */
    private static String email;
    /**
     * User's real name (can be empty).
     */
    private static String realName;

    // Game Info
    /**
     * User's username, used as a primary key in the Shoudo Database.
     */
    private static String username;
    /**
     * Games won by this user over the history of their account. Includes
     * victories by opponent concession. Used to calculate win percentage.
     */
    private static Integer gamesWon;
    /**
     * Games played by this user over the history of their account. Used to
     * calculate win percentage.
     */
    private static Integer gamesPlayed;
    /**
     * Total armies destroyed in combat by this user over the history of their
     * account.
     */
    private static Integer armiesDestroyed;
    /**
     * Total lands burned by this user over the history of their account.
     */
    private static Integer landsBurned;
    /**
     * Total wins by subjugation (capturing opponent's Daimyo castle) by this
     * user over the history of their account. This is the default win for if
     * an opponent concedes.
     */
    private static Integer subjugationWins;
    /**
     * Total wins by conquest (destroying all of opponent's armies) by this
     * user over the history of their account.
     */
    private static Integer conquestWins;

    // Undisplayed
    /**
     * Total games played as the Uesugi clan by this user over the history of
     * their account.
     */
    private static Integer gamesPlayedUesugi;
    /**
     * Total games played as the Mori clan by this user over the history of
     * their account.
     */
    private static Integer gamesPlayedMori;
    /**
     * Total games played as the Tokugawa clan by this user over the history of
     * their account.
     */
    private static Integer gamesPlayedTokugawa;
    /**
     * Total games played as the Shimazu clan by this user over the history of
     * their account.
     */
    private static Integer gamesPlayedShimazu;

    /**
     * Static final value for window width.
     */
    private static final double WIDTH = 400f;
    /**
     * Static final value for window height.
     */
    private static final double HEIGHT = 600f;
    /**
     * Static final value for gridpane insets.
     */
    private static final double INSET = 10f;
    /**
     * Static final value for gridpane vertical gap.
     */
    private static final double VGAP = 8f;
    /**
     * Static final value for gridpane horizontal gap.
     */
    private static final double HGAP = 10f;
    /**
     * Static final multiplier to convert decimal to percent.
     */
    private static final double PERCENT = 100f;

    /**
     * Static final value of 0 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL0 = 0;
    /**
     * Static final value of 1 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL1 = 1;
    /**
     * Static final value of 2 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL2 = 2;
    /**
     * Static final value of 3 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL3 = 3;
    /**
     * Static final value of 4 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL4 = 4;
    /**
     * Static final value of 5 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL5 = 5;
    /**
     * Static final value of 6 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL6 = 6;
    /**
     * Static final value of 7 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL7 = 7;
    /**
     * Static final value of 8 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL8 = 8;
    /**
     * Static final value of 9 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL9 = 9;
    /**
     * Static final value of 10 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL10 = 10;
    /**
     * Static final value of 11 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL11 = 11;
    /**
     * Static final value of 12 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL12 = 12;
    /**
     * Static final value of 13 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL13 = 13;
    /**
     * Static final value of 14 for column and row indexing to comply with java
     * standards.
     */
    private static final int VAL14 = 14;

    /**
     * Current user's win percentage, calculated by dividing
     * {@link AccountDetailsWindow#gamesWon} by
     * {@link AccountDetailsWindow#gamesPlayed} and multiplying by
     * {@link AccountDetailsWindow#PERCENT}.
     */
    private static double winPercent;

    /**
     * Private constructor to prevent accidental or malicious initialization of
     * non-initializable window.
     */
    private AccountDetailsWindow() {

    }

    /**
     * Displays the account details window, loading all user values and
     * presenting them in a grid.
     *
     * @param u user to display details for
     */
    public static void display(final User u) {
        user = u;

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        window.setTitle("Account details for user: " + user.getUsername());

        window.setMinWidth(WIDTH);
        window.setMinHeight(HEIGHT);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(INSET, INSET, INSET, INSET));
        grid.setVgap(VGAP);
        grid.setHgap(HGAP);

        if (gamesPlayed != null) {
            if (gamesPlayed.equals(0)) {
                winPercent = 0f;
            } else {
                winPercent = (((double) gamesWon) / ((double) gamesPlayed))
                        * PERCENT;
            }
        } else {
            winPercent = 0f;
        }

        loadParams();

        // Labels
        Label userNameLabel = new Label("Username:");
        Label realNameLabel = new Label("Real Name:");
        Label emailLabel = new Label("Email:");
        Label mostPlayedClanLabel = new Label("Favorite Clan:");
        Label gamesWonLabel = new Label("Games Won:");
        Label gamesPlayedLabel = new Label("Games Played:");
        Label winPercentLabel = new Label("Win Percentage:");
        Label conquestWinsLabel = new Label("Conquest Wins");
        Label subjugationWinsLabel = new Label("Subjugation Wins");
        Label armiesDestroyedLabel = new Label("Armies Destroyed:");
        Label landsBurnedLabel = new Label("Lands Burned:");
        Label uesugiGamesLabel = new Label("Games as Uesugi:");
        Label moriGamesLabel = new Label("Games as Mori:");
        Label tokugawaGamesLabel = new Label("Games as Tokugawa:");
        Label shimazuGamesLabel = new Label("Games as Shimazu:");

        // Fields
        Label userNameField = new Label(username);
        Label realNameField = new Label(realName);
        Label emailField = new Label(email);
        Label mostPlayedClanField = new Label(user.getMostPlayedClan());
        Label gamesWonField = new Label(Integer.toString(gamesWon));
        Label gamesPlayedField = new Label(Integer.toString(gamesPlayed));
        Label winPercentField = new Label(Double.toString(winPercent) + "%");
        Label conquestWinsField = new Label(Integer.toString(conquestWins));
        Label subjugationWinsField = new Label(
                Integer.toString(subjugationWins));
        Label armiesDestroyedField = new Label(
                Integer.toString(armiesDestroyed));
        Label landsBurnedField = new Label(
                Integer.toString(landsBurned));
        Label uesugiGamesField = new Label(
                Integer.toString(gamesPlayedUesugi));
        Label moriGamesField = new Label(
                Integer.toString(gamesPlayedMori));
        Label tokugawaGamesField = new Label(
                Integer.toString(gamesPlayedTokugawa));
        Label shimazuGamesField = new Label(
                Integer.toString(gamesPlayedShimazu));

        // Load Labels/Fields into grid
        GridPane.setConstraints(userNameLabel, VAL0, VAL0);
        GridPane.setConstraints(realNameLabel, VAL0, VAL1);
        GridPane.setConstraints(emailLabel, VAL0, VAL2);
        GridPane.setConstraints(mostPlayedClanLabel, VAL0, VAL3);
        GridPane.setConstraints(gamesWonLabel, VAL0, VAL4);
        GridPane.setConstraints(gamesPlayedLabel, VAL0, VAL5);
        GridPane.setConstraints(winPercentLabel, VAL0, VAL6);
        GridPane.setConstraints(conquestWinsLabel, VAL0, VAL7);
        GridPane.setConstraints(subjugationWinsLabel, VAL0, VAL8);
        GridPane.setConstraints(armiesDestroyedLabel, VAL0, VAL9);
        GridPane.setConstraints(landsBurnedLabel, VAL0, VAL10);
        GridPane.setConstraints(uesugiGamesLabel, VAL0, VAL11);
        GridPane.setConstraints(moriGamesLabel, VAL0, VAL12);
        GridPane.setConstraints(tokugawaGamesLabel, VAL0, VAL13);
        GridPane.setConstraints(shimazuGamesLabel, VAL0, VAL14);

        GridPane.setConstraints(userNameField, VAL1, VAL0);
        GridPane.setConstraints(realNameField, VAL1, VAL1);
        GridPane.setConstraints(emailField, VAL1, VAL2);
        GridPane.setConstraints(mostPlayedClanField, VAL1, VAL3);
        GridPane.setConstraints(gamesWonField, VAL1, VAL4);
        GridPane.setConstraints(gamesPlayedField, VAL1, VAL5);
        GridPane.setConstraints(winPercentField, VAL1, VAL6);
        GridPane.setConstraints(conquestWinsField, VAL1, VAL7);
        GridPane.setConstraints(subjugationWinsField, VAL1, VAL8);
        GridPane.setConstraints(armiesDestroyedField, VAL1, VAL9);
        GridPane.setConstraints(landsBurnedField, VAL1, VAL10);
        GridPane.setConstraints(uesugiGamesField, VAL1, VAL11);
        GridPane.setConstraints(moriGamesField, VAL1, VAL12);
        GridPane.setConstraints(tokugawaGamesField, VAL1, VAL13);
        GridPane.setConstraints(shimazuGamesField, VAL1, VAL14);

        grid.getChildren().addAll(userNameField,
                realNameField, emailField, gamesWonField,
                gamesPlayedField, winPercentField,
                conquestWinsField, subjugationWinsField,
                uesugiGamesField, moriGamesField, tokugawaGamesField,
                shimazuGamesField, armiesDestroyedField,
                landsBurnedField, mostPlayedClanField,
                userNameLabel, realNameLabel,
                emailLabel, gamesWonLabel, gamesPlayedLabel,
                winPercentLabel, conquestWinsLabel, subjugationWinsLabel,
                uesugiGamesLabel, moriGamesLabel, tokugawaGamesLabel,
                shimazuGamesLabel, armiesDestroyedLabel,
                landsBurnedLabel, mostPlayedClanLabel);
        grid.setAlignment(Pos.CENTER);

        Scene accountDetails = new Scene(grid, WIDTH, HEIGHT);
        accountDetails.getStylesheets().add(Main.class.getResource(
                "/styling/mainMenu1.css").toExternalForm());
        window.setScene(accountDetails);
        window.showAndWait();
    }

    /**
     * Loads params from the user to display.
     */
    private static void loadParams() {
        username = user.getUsername();
        email = user.getEmail();
        realName = user.getRealName();
        gamesWon = user.getGamesWon();
        gamesPlayed = user.getGamesPlayed();
        conquestWins = user.getConquestWins();
        subjugationWins = user.getSubjugationWins();
        gamesPlayedUesugi = user.getGamesPlayedUesugi();
        gamesPlayedMori = user.getGamesPlayedMori();
        gamesPlayedTokugawa = user.getGamesPlayedTokugawa();
        gamesPlayedShimazu = user.getGamesPlayedShimazu();
        armiesDestroyed = user.getArmiesDestroyed();
        landsBurned = user.getLandsBurned();
    }

}

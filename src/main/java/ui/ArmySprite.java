package ui;

import shoudo.Army;

/**
 * Extends the {@link Sprite} class, adding functionality specific to army
 * pieces. This includes a private army class and actionTaken boolean, and
 * the ability to interface with the army class.
 *
 * @author JR Diehl
 * @version 1.0
 */
public class ArmySprite extends Sprite {

    /**
     * Army instance which controls game information about the army that is
     * not UI-related.
     */
    private Army army;
    /**
     * Stores whether the army has taken its action this turn.
     */
    private boolean actionTaken = false;

    /**
     * @return {@link ArmySprite#army}
     */
    public final Army getArmy() {
        return army;
    }

    /**
     * Sets the internal Army to a new value.
     *
     * @param a the new internal Army value
     */
    public final void setArmy(final Army a) {
        this.army = a;
    }

    /**
     * @return {@link ArmySprite#actionTaken}
     */
    public final boolean getActionTaken() {
        return actionTaken;
    }

    /**
     * Sets whether the sprite has taken its action for the turn (attack, burn,
     * or restore). Typically reset after each turn to false and set to true
     * whenever an action is performed.
     *
     * @param at the new valule for actionTaken
     */
    public final void setActionTaken(final boolean at) {
        this.actionTaken = at;
    }
}

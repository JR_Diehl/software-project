package ui;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import shoudo.Card;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Extends the {@link Sprite} class with functionality to represent a card on
 * the game board. This includes storing multiple Image objects (active,
 * burned, and unburned), as well as other necessary information to display the
 * card on the board.
 *
 * @author JR Diehl
 * @version  1.0
 */
public class CardSprite extends Sprite {

    /**
     * Internal card object to access gameplay specific card info.
     */
    private Card card;
    /**
     * Card's burned image, loaded from the burned image path of the internal
     * card object.
     */
    private Image burnedImage;
    /**
     * Card's unburned image, loaded from the unburned image path of the
     * internal card object. This is the default image that all cards load
     * with.
     */
    private Image unburnedImage;
    /**
     * The CardSprite's current overlay mode. The overlay is used during the
     * SELECT state to show possible moves and actions.
     */
    private String overlayMode = "off";
    /**
     * The overlay image for possible attacks and burns.
     */
    private final Image attackOverlay = new Image(Main.class.getResource(
            "/images/overlay/attack_overlay.png").toExternalForm());
    /**
     * The overlay image for possible moves.
     */
    private final Image moveOverlay = new Image(Main.class.getResource(
            "/images/overlay/move_overlay.png").toExternalForm());
    /**
     * The overlay image for possible restores.
     */
    private final Image restoreOverlay = new Image(Main.class.getResource(
            "/images/overlay/restore_overlay.png").toExternalForm());
    /**
     * The CardSprite's row coordinate. From 0 to 4.
     */
    private int row;
    /**
     * The CardSprite's column coordinate. From 0 to 7.
     */
    private int col;

    /**
     * Flips the CardSprite to its opposite side. Only Shrine cards can flip
     * from burned side back to unburned side; otherwise, the transition is
     * one-way.
     *
     * @param gc the GraphicsContext to draw to
     */
    public final void flip(final GraphicsContext gc) {
        if (this.getImage().equals(this.unburnedImage)) {
            this.setImage(this.burnedImage);
        } else if (this.card.getPieceSupported().equals("Shrine")) {
            this.setImage(this.unburnedImage);
        }
        this.render(gc);
    }

    /**
     * Renders the card with possible overlays based on overlay mode.
     *
     * @param gc the GraphicsContext to draw to
     */
    @Override
    public final void render(final GraphicsContext gc) {
        super.render(gc);
        if (this.overlayMode.equals("attack")) {
            gc.drawImage(this.attackOverlay, this.getPosX(),
                    this.getPosY(), this.getWidth(), this.getHeight());
        } else if (this.overlayMode.equals("move")) {
            gc.drawImage(this.moveOverlay, this.getPosX(),
                    this.getPosY(), this.getWidth(), this.getHeight());
        } else if (this.overlayMode.equals("restore")) {
            gc.drawImage(this.restoreOverlay, this.getPosX(),
                    this.getPosY(), this.getWidth(), this.getHeight());
        }
    }

    /**
     * Gets all adjacent CardSprites to this object as a list. Adjacency is
     * checked only in the row-only or column-only dimensions; diagonal
     * adjacency is not defined.
     *
     * @param board the board to check adjacency in
     * @return a list of adjacent CardSprite objects
     */
    private List<CardSprite> getAdjacent(final List<CardSprite> board) {
        return board
                .stream()
                .filter(x -> (x.col == this.col + 1 && x.row == this.row)
                        || (x.col == this.col - 1 && x.row == this.row)
                        || (x.row == this.row + 1 && x.col == this.col)
                        || (x.row == this.row - 1 && x.col == this.col))
                .collect(Collectors.toList());
    }

    /**
     * Gets a list of CardSprites which can be legally moved to from this
     * CardSprite. A move is valid if the CardSprite does not have an
     * ArmySprite occupying its space and is not a castle that does not
     * belong to the moving player's clan.
     *
     * @param board the board to check for valid moves on
     * @param armies the armies on the board
     * @param clan the moving player's clan
     * @return a list of CardSprite objects that are valid moves
     */
    final List<CardSprite> getValidMoves(final List<CardSprite> board,
                                   final List<ArmySprite> armies,
                                   final String clan) {
        return getAdjacent(board)
                .stream()
                .filter(x -> (!x.card.getIsCastle()
                        || (x.getImage().equals(x.burnedImage)
                        || x.card.getClan().equals(clan)))
                        && armies
                                .stream()
                        .noneMatch(a ->
                                a.getArmy().getCol()
                                        == x.col
                                        && a.getArmy().getRow() == x.row))
                .collect(Collectors.toList());
    }

    /**
     * Gets a list of CardSprites that have valid actions which can be done to
     * them from this CardSprite. This includes burning, restoring, and
     * attacking.
     *
     * @param board the board to be checked
     * @param armies the armies on the board
     * @param clan the acting player's clan
     * @return a list of CardSprites that can be acted upon
     */
    final List<CardSprite> getActions(final List<CardSprite> board,
                                final List<ArmySprite> armies,
                                final String clan) {
        List<CardSprite> ret = getAdjacent(board)
                .stream()
                .filter(x -> (x.card.getIsCastle()
                        && !x.card.getClan().equals(clan)
                        && x.getImage().equals(x.unburnedImage))
                        || armies
                                .stream().anyMatch(a ->
                                a.getArmy().getCol() == x.col
                                        && a.getArmy().getRow() == x.row
                                        && !a.getArmy().getOwner().getClan()
                                        .equals(clan)))
                .collect(Collectors.toList());
        if ((this.card.getPieceSupported().equals("Shrine")
                && this.getImage().equals(this.burnedImage)
                && this.card.getClan().equals(clan))
                || (!this.card.getClan().equals(clan)
                && !this.getImage().equals(this.burnedImage))) {
            ret.add(this);
        }

        return ret;
    }

    /**
     * @return {@link CardSprite#card}
     */
    public final Card getCard() {
        return card;
    }

    /**
     * Sets the internal card object.
     *
     * @param c the new card object
     */
    public final void setCard(final Card c) {
        this.card = c;
    }

    /**
     * @return {@link CardSprite#burnedImage}
     */
    public final Image getBurnedImage() {
        return burnedImage;
    }

    /**
     * Sets the burned image.
     *
     * @param bi the new burned image
     */
    public final void setBurnedImage(final Image bi) {
        this.burnedImage = bi;
    }

    /**
     * @return {@link CardSprite#unburnedImage}
     */
    public final Image getUnburnedImage() {
        return unburnedImage;
    }

    /**
     * Sets the unburned image.
     *
     * @param ui new unburned image
     */
    public final void setUnburnedImage(final Image ui) {
        this.unburnedImage = ui;
    }

    /**
     * Sets the overlay mode.
     *
     * @param om new overlay mode
     */
    public final void setOverlayMode(final String om) {
        this.overlayMode = om;
    }

    /**
     * @return {@link CardSprite#row}
     */
    public final int getRow() {
        return row;
    }

    /**
     * Sets the row.
     *
     * @param r new row
     */
    public final void setRow(final int r) {
        this.row = r;
    }

    /**
     * @return {@link CardSprite#col}
     */
    public final int getCol() {
        return col;
    }

    /**
     * Sets the col.
     *
     * @param c new col
     */
    public final void setCol(final int c) {
        this.col = c;
    }
}

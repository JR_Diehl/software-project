package ui;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Class used to render graphics to the canvas during gameplay. It is extended
 * by {@link ArmySprite} and {@link CardSprite}, which implement its behavior
 * for specific cases. This class is not explicitly used significantly in the
 * application; it simply allows for the polymorphic behavior present in
 * ArmySprite and CardSprite.
 *
 * @author JR Diehl
 * @version 1.0
 */
class Sprite {

    /**
     * The image to be rendered to the screen.
     */
    private Image image;
    /**
     * The sprite's x position on the screen.
     */
    private double posX;
    /**
     * The sprite's y position on the screen.
     */
    private double posY;
    /**
     * The sprite's x velocity. Used to control the speed and direction of
     * movements.
     */
    private double velX;
    /**
     * The sprite's y velocity. Used to control the speed and direction of
     * movements.
     */
    private double velY;
    /**
     * The sprite's width. Used with its x position to draw the object easily.
     */
    private double width;
    /**
     * The sprite's height. Used with its y position to draw the object easily.
     */
    private double height;
    /**
     * Time spent moving by the sprite. This is used to track the duration of
     * movement animations and prevent them from becoming infinite due to
     * issues with double equality.
     */
    private double timeMoving;
    /**
     * Time this sprite will move for. This is used to track the duration of
     * movement animations and prevent them from becoming infinite due to
     * issues with double equality.
     */
    private double timeToMove;
    /**
     * X destination of current move.
     */
    private double destX;
    /**
     * Y destination of current move.
     */
    private double destY;

    /**
     * Updates the sprites position using its velocity. Also checks to see
     * if move is complete based on timeMoving/toMove and destX and destY,
     * stopping movement if necessary.
     *
     * @param time time that has elapsed since last update
     */
    public void update(final double time) {
        if (this.timeMoving < this.timeToMove) {
            this.timeMoving += time;

            posX += velX * time;
            posY += velY * time;

            if (timeMoving >= timeToMove) {
                posX = destX;
            }
            if (timeMoving >= timeToMove) {
                posY = destY;
            }
        } else {
            this.velY = 0;
            this.velX = 0;
            this.destX = 0;
            this.destY = 0;
        }
    }

    /**
     * Renders the sprite to the given GraphicsContext.
     *
     * @param gc the GraphicsContext to draw to
     */
    public void render(final GraphicsContext gc) {
        gc.drawImage(image, posX, posY, width, height);
    }

    /**
     * Gets the boundary of the sprite; useful for collision and mouseclick
     * detection.
     *
     * @return Rectangle2D representing space occupied by sprite
     */
    public Rectangle2D getBoundary() {
        return new Rectangle2D(posX, posY, width, height);
    }

    /**
     * Initiates a move to the given x and y in the given time.
     *
     * @param x x coordinate to move to
     * @param y y coordinate to move to
     * @param time time to move for
     */
    public void move(final double x,
                     final double y,
                     final double time) {
        this.velX = (x - this.posX) / time;
        this.velY = (y - this.posY) / time;
        this.timeToMove = time;
        this.timeMoving = 0;
        this.destX = x;
        this.destY = y;
    }

    /**
     * @return {@link Sprite#image}
     */
    public Image getImage() {
        return image;
    }

    /**
     * Sets the sprite's image.
     *
     * @param i new image
     */
    public void setImage(final Image i) {
        this.image = i;
    }

    /**
     * @return {@link Sprite#posX}
     */
    public double getPosX() {
        return posX;
    }

    /**
     * Sets x position.
     *
     * @param x new x position
     */
    public void setPosX(final double x) {
        this.posX = x;
    }

    /**
     * @return {@link Sprite#posY}
     */
    public double getPosY() {
        return posY;
    }

    /**
     * Set y position.
     *
     * @param y new y position
     */
    public void setPosY(final double y) {
        this.posY = y;
    }

    /**
     * Set x velocity.
     *
     * @param x new x velocity
     */
    public void setVelX(final double x) {
        this.velX = x;
    }

    /**
     * Set y velocity.
     *
     * @param y new y velocity
     */
    public void setVelY(final double y) {
        this.velY = y;
    }

    /**
     * @return {@link Sprite#width}
     */
    public double getWidth() {
        return width;
    }

    /**
     * Set width.
     *
     * @param w new width
     */
    public void setWidth(final double w) {
        this.width = w;
    }

    /**
     * @return {@link Sprite#height}
     */
    public double getHeight() {
        return height;
    }

    /**
     * Set height.
     *
     * @param h new height
     */
    public void setHeight(final double h) {
        this.height = h;
    }
}

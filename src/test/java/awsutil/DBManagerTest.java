package awsutil;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This test suite ensures proper functionality of the Shoudo Database, which
 * is the primary component of this application that is feasibly testable
 * through unit testing.
 *
 * @author JR Diehl
 * @version 1.0
 */
class DBManagerTest {

    /**
     * Dummy user for testing purposes. Default and easy to handle.
     */
    private static User dummyUser;

    /**
     * Creates a dummy user and saves them to the database.
     */
    @BeforeEach
    void setup() {
        dummyUser = new User("dummy", "password");
        DBManager.save(dummyUser, "users");
    }

    /**
     * Deletes the dummy user from the table.
     */
    @AfterEach
    void teardown() {
        DBManager.deleteItem(dummyUser, "users");
    }

    /**
     * Ensures that getting an item that has just been saved to the database
     * is successful.
     */
    @Test
    void getItemGood() {

        String username = dummyUser.getUsername();

        assertTrue(DBManager.getItem(username, "users") != null);
    }

    /**
     * Ensures that attempting to get a user with empty username from the
     * database returns null as it should.
     */
    @Test
    void getItemBad() {

        String username = dummyUser.getUsername();

        assertTrue(DBManager.getItem("", "users") == null);
    }

    /**
     * Ensures that there is not a collision when inserting a second dummy
     * user into the database with a different username.
     */
    @Test
    void saveNoCollide() {

        User dummy2 = new User("dummy2", "password");

        assertTrue(DBManager.save(dummy2, "users"));

        DBManager.deleteItem(dummy2, "users");
    }

    /**
     * Ensures that a collision occurs when trying to save dummyUser to the
     * database a second time.
     */
    @Test
    void saveCollide() {
        assertFalse(DBManager.save(dummyUser, "users"));
    }
}
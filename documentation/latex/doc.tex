\documentclass[12pt,letterpaper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{float}
\usepackage{hyperref}

\author{J.R. Diehl}
\title{Shoudo: the Scorched Earth Game}

\begin{document}
\pagenumbering{gobble}

\maketitle

\vspace{80mm}

\section*{Outline}

This application is a desktop video game implementation of the board game Shoudo, which was designed and developed by
Houseplant Games in 2017 and launched on Kickstarter in 2018. The application allows users to create accounts which are 
stored in a remote database and which allow them to log in to the application with a username and password. Once they have logged in,
users can access their account details to view information about their account such as username, email, and various game-related
statistics. They can also play local multiplayer games against an opponent who is using the same computer. This opponent does not need
to have an account to play, nor will their statistics from the game be stored.

\section*{Vision Statement}

My goal in creating this application was to recreate the experience of playing the board game Shoudo as faithfully as possible in a
video game environment, while also providing utilities that only a video game could provide, in particular tracking of gameplay statistics
for each user so they would be able to see how they were performing as they played.

\pagebreak

\section*{Roles}

\begin{itemize}
\item Potential User: a person that is attempting to create an account
\item User: a registered user for the application. Can log in, play local games, and save and load account statistics from a database
\item Player: a player in a local game. Can have a user, which stores statistics about the game being played.
\end{itemize}

\section*{Project Timeline}

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{Shoudo-gantt-diagram.jpg}
  \caption{Project Gantt Diagram}
  \label{fig:project-gantt-diagram}
\end{figure}

The development of this project was delayed due to commitments with other classes, as well as other extracurricular activities. Thus,
the development lifecycle had to be compressed, and large amounts of work were done in short periods of time. This led to each of the
three major development phases being broader in scope than they otherwise would have been, as there was neither time nor reason to
make them more granular due to the vast quantity of work that needed to be completed during each phase.

\section*{Requirements}

\begin{enumerate}
\item A database must store user information, including but not limited to username, real name, email, password (encrypted), and various gameplay statistics
\item The database must be remotely accessible.
\item The database must be authenticated using access keys stored in the application code.
\item A user interface must be created that controls access to application functionality and information.
\item A state machine must be created to control the local gameplay.
\item The user interface must update itself based on changing game information.
\end{enumerate}

\section*{Business Rules}

\begin{enumerate}
\item Users must have unique usernames.
\item A user should be able to create an account which can be used to log into the application on any device running it via my database.
\item A user should be able to access statistics about their account from the main application via my database.
\item A user should be able to play local games with another player on their device while logged in to the application.
\end{enumerate}

\section*{Use Cases}

\begin{enumerate}
\item \textbf{User Creates Account}

\textbf{Scope} Shoudo Database

\textbf{Level} user goal

\textbf{Stakeholders and Interests}

Potential User

- person that is interested in creating an account in the Shoudo system

\textbf{Precondition:} Shoudo Database exists

\textbf{Postcondition:} Potential User is registered to system as a User

\textbf{Main success scenario:}

\begin{enumerate}
\item potential user wants to create an account

\item user opens register account window

\item potential user enters username and password, and possibly real name and/or email

\item system saves potential user to system as new user
\end{enumerate}

\textbf{Extensions:}

*.1 application fails to connect to the database

\hspace{3mm}a. application will create a warning dialog alerting potential user

c.1 potential user does not enter one or both of username and password

\hspace{3mm}a. application will create a warning dialog alerting potential user

d.1 a user with the entered username already exists

\hspace{3mm}a. application will create a warning dialog alerting potential user

\pagebreak

\item \textbf{User logs in to system}

\textbf{Scope} Shoudo Database

\textbf{Level} user goal

\textbf{Stakeholders and Interests}

User

- person that is interested in logging in to an account in the Shoudo system

\textbf{Precondition:} Shoudo Database exists and user has a registered account

\textbf{Postcondition:} User is logged in to the system

\textbf{Main success scenario:}

\begin{enumerate}
\item user wants to log in to system

\item user enters username and password

\item system checks to see if user is in system

\item system checks to see that password is correct

\item user is logged in to system
\end{enumerate}

\textbf{Extensions:}

*.1 application fails to connect to the database

\hspace{3mm}a. application will create a warning dialog alerting user

c.1 user is not registered in system

\hspace{3mm}a. application will create a warning dialog alerting potential user

d.1 password is not correct

\hspace{3mm}a. application will create a warning dialog alerting potential user

\pagebreak

\item \textbf{User plays a game}

\textbf{Scope} Shoudo Application

\textbf{Level} user goal

\textbf{Stakeholders and Interests}

Player

- User that wants to play a game of Shoudo with an Opponent

Opponent

- person that is not logged in as a User, that will play against the Player

\textbf{Precondition:} Player is logged in to Shoudo Database

\textbf{Postcondition:} Player and Opponent have played a game of Shoudo, and one has won

\textbf{Main success scenario:}

\begin{enumerate}
\item player and opponent want to play a game of Shoudo

\item application loads a new game, creating a random board and assigning a random clan to player and opponent

\item first player takes turn

\item second player takes turn

\item if neither player has won, return to (c)
\end{enumerate}

\textbf{Extensions:}

e.1 player has won

\hspace{3mm}a. application will print a dialog announcing the winner

\hspace{3mm}b. application will update player's gameplay statistics

e.2 opponent has won

\hspace{3mm}a. application will print a dialog announcing the winne
\end{enumerate}

\pagebreak

\section*{Use Case Diagram}

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{primary-use-cases.png}
  \caption{Primary Use Cases}
  \label{fig:project-use-cases}
\end{figure}

\section*{System Sequence Diagram}

\begin{figure}[H]
  \centering
  \includegraphics[width=400px]{system-sequence.png}
  \caption{System Sequence Diagram}
  \label{fig:system-sequence}
\end{figure}

\section*{System Operations}

\begin{itemize}

\item Register

A potential user registers to the system

\item Login

A user logs in to the system

\item Save statistics

A user's game statistics are save to or updated in the system

\item Verify password

The system verifies the user's password

\item Verify username uniqueness

The system verifies the uniqueness of the username entered

\item Play local game

The user plays a game with a local opponent

\end{itemize}

\section*{Domain Model}

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{domain-model.png}
  \caption{Project Domain Model}
  \label{fig:domain-model}
\end{figure}

\pagebreak

\section*{Operation Contracts}

\begin{itemize}

\item \textbf{Register}

\textit{Precondition}: user must have a valid username and password prepared

\textit{Postcondition}: user is registered to system

\item \textbf{Login}

\textit{Precondition}: user must have a registered account and know login credentials

\textit{Postcondition}: user is logged in to the system

\item \textbf{Save statistics}

\textit{Precondition}: user must be logged in to the system and have just finished a game

\textit{Postcondition}: user's updated statistics will be stored in the Shoudo Database

\item \textbf{Verify password}

\textit{Precondition}: a password must have been entered for a valid username

\textit{Postcondition}: the correctness of the password will be returned

\item \textbf{Verify username uniqueness}

\textit{Precondition}: a username must have been entered

\textit{Postcondition}: the username will be checked for uniqueness in the Shoudo Database

\item \textbf{Play local game}

\textit{Precondition}: user must be logged in to system

\textit{Postcondition}: user will play a local game with an opponent who is not a registered user and the user's statistics will be recorded after the game

\end{itemize}

\pagebreak

\section*{Sequence Diagrams}

\begin{enumerate}
\item \textbf{Register}

\begin{figure}[H]
  \centering
  \includegraphics[width=400px]{register-sequence.png}
  \caption{Register Sequence Diagram}
  \label{fig:register-sequence}
\end{figure}

\pagebreak

\item \textbf{Login}

\begin{figure}[H]
  \centering
  \includegraphics[width=400px]{login-sequence.png}
  \caption{Login Sequence Diagram}
  \label{fig:login-sequence}
\end{figure}

\pagebreak

\item \textbf{Play Game}

\begin{figure}[H]
  \centering
  \includegraphics[width=400px]{play-game-sequence.png}
  \caption{Play Game Sequence Diagram}
  \label{fig:play-game-sequence}
\end{figure}

\end{enumerate}

\section*{Design Model}

\begin{figure}[H]
  \centering
  \includegraphics[width=380px]{entire_project_diagram.png}
  \caption{Project Design Model}
  \label{fig:project-design-model}
\end{figure}

The design model above was generated using IntelliJ's code analysis tools to create a diagram of all three packages used in the application with
all of their relationships and dependencies, as well as all fields, methods, and properties.

\section*{Justification of GRASPS}

This project maintains high cohesion by separating its functionality into three packages -- Shoudo, ui, and awsutil. The Shoudo package contains classes which are information experts about various aspects of Shoudo gameplay, the ui package contains multiple different elements which control the user interface and some of the high level gameplay management for the Shoudo game, and the awsutil package interfaces with the users database and acts as a creator for user objects. This also supported low coupling, as connections between classes were only necessary to access specific functions and information which any given class
was lacking.

\vspace{3mm}

Polymorphism was used particularly in ui to create sprites which all shared a similar set of basic functions to allow for ease of rendering and movement around the game board. Additionally, the GameWindow class in ui acts as a controller for Shoudo games, managing the high level gameplay and interfacing with the information experts of the Shoudo package to regulate the game.

\section*{Design Patterns Used}

\begin{enumerate}

\item \textbf{Singleton} -- GameRunner class

The GameRunner class is made to be a singleton because it controls the state of the current Shoudo game being played, and only one game should be running at a single time. It also allows for easy exiting and resetting of the game state by simply using the static killInstance() method to nullify the currently active instance.

\item \textbf{Iterator} -- when determining selected card

An iterator was used here to iterate over multiple CardSprite objects.

\item \textbf{Builder} -- Player class

The player class has a builder because it allowed for easy creation of the players during game initialization.

\item \textbf{Command} -- javafx buttonClick and onMouseClick actions

Commands are used to control the actions executed on various events in the ui because they allow for varied and dynamic behavior in response to user actions. Changing the action during different states also allows for smooth transitioning between game functionality in different states.

\item \textbf{Lazy Initialization} -- GameRunner class

The instance of game runner is not created until the getInstance() method is called to prevent the unnecessary creation of a class before it is needed.

\end{enumerate}

\section*{Unit Tests}

The use cases for this application were not feasible to test directly with unit testing; however, for the Register and Login use cases they could be indirectly tested by testing the underlying database operations. To accomplish this, tests were implemented to confirm that the save function worked as intended both when submitting a unique save and a colliding save. Additionally, tests were implemented to ensure that the getItem method functioned correctly when asked for an item that did exist within the database and one that did not. 

\vspace{3mm}

For the third use case -- Play Game -- unit testing is not appropriate. Instead, the game was rigorously tested manually and shown to others to ensure that it fulfilled the intended requirements correctly. If additional play testing is deemed necessary, the rules of the game can be found in /documentation/game-rules/shoudo-rules.pdf. These rules are not exactly implemented in the desktop application,  but they are almost entirely ported over. The key differences are listed below:

\begin{itemize}

\item Clan powers are not used

\item Armies retreat at random, rather than choosing where to retreat.

\end{itemize}

\section*{Project Statistics}

The time I spent working on this project (not including analysis and design) is delineated below:

\begin{itemize}

\item Wednesday, 04/18/2018: 3 hrs

\item Thursday, 04/19/2018: 2 hrs

\item Friday, 04/20/2018: 5 hrs

\item Saturday, 04/21/2018: 8 hrs

\item Sunday, 04/22/2018: 11 hrs

\item Monday, 04/23/2018: 14 hrs

\item Tuesday, 04/24/2018: 13 hrs

\item Wednesday, 04/25/2018: 18 hrs

\item Thursday, 04/26/2018: 7 hrs

\end{itemize}

In total, 81 hours were spent developing the code for this application from April 18th, 2018 to April 26th, 2018.

The git repository can be found at the following url: \url{https://bitbucket.org/JR_Diehl/software-project/src/master/}

The project has 2,538 lines of logical Java code and 1,856 lines of comments. The full cloc-1.76 report is shown below:

\begin{figure}[H]
  \centering
  \includegraphics[width=430px]{cloc-report.png}
  \caption{cloc-1.76 Report}
  \label{fig:cloc-reportl}
\end{figure}

\end{document}